import socket
import sys
import rsa
import base64


def getMessage(sock):
	buff = ""
	while True:			
		data = sock.recv(2048)
		if not data:
			return None

		append = str(data.decode("utf-8", "ignore"))
		buff += append

		if append.endswith("\n\r\n\r") or append.endswith("\r\n\r\n"):
			buff = buff[:-4]
			break
		pass

	return buff


HOST = '127.0.0.1'
PORT = 8888

clientSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	
clientSock.connect((HOST, PORT))

print("Loading keys")
f = open("pubkey.pem", "r")
publicKey = rsa.PublicKey.load_pkcs1(f.read());
f.close();

crypto = rsa.encrypt("strunzz".encode("utf-8"), publicKey)
print(base64.b64encode(crypto).decode("utf-8"))


s = '{ "message": "' + base64.b64encode(crypto).decode("utf-8") + '", "error": null }\n\r\n\r'
clientSock.send(s.encode('utf-8', 'ignore'))

print(getMessage(clientSock))

clientSock.close();