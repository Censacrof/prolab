import socket
import sys
import threading
import json
import base64
from enum import Enum

import rsa #pip install rsa

#aggiungo metodo a Object per la serializzazione in json
class Object:
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)

class ConnectionState(Enum):
		ACCEPTED = 1
		KEYEXCHANGE = 2

class AuthServer:
	HOST = ''
	PORT = 8888

	serverSocket = None

	publicKey = None
	privateKey = None

	def __init__(self):
		print("Initializing socket...")

		self.serverSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		try:
			self.serverSock.bind((self.HOST, self.PORT))
		except socket.error as msg:
			print("Cant bind socket. Error: " + msg[0] + "\n\r\n\r" + msg[1]);
			sys.exit();

		print("Socket bound")

		print("Loading keys")
		f = open("pubkey.pem", "r")
		self.publicKey = rsa.PublicKey.load_pkcs1(f.read());
		f.close();

		f = open("privkey.pem", "r")
		self.privateKey = rsa.PrivateKey.load_pkcs1(f.read());
		f.close();

		pass


	def listen(self):
		self.serverSock.listen(10);
		print("Socket listening")

		while True:
			clientSocket, addr = self.serverSock.accept()
			a = addr[0] + ":" + str(addr[1])

			print ("Connected with: " + a)
			threading._start_new_thread(self.accept, (clientSocket, a));
			pass
		pass


	def getMessage(self, sock):
		buff = ""
		while True:			
			data = sock.recv(2048)
			if not data:
				return None

			append = str(data.decode("utf-8", "ignore"))
			buff += append

			if append.endswith("\n\r\n\r") or append.endswith("\r\n\r\n"):
				buff = buff[:-4]
				break
			pass

		return buff


	def sendMessage(self, sock, message, error = None):
		if error is None:
			error = "null"

		s = '{ "message": "' + message + '", "error": ' + str(error) + ' }\n\r\n\r'
		sock.send(s.encode('utf-8', 'ignore'))
		pass


	def accept(self, sock, addr):
		connectionState = ConnectionState["ACCEPTED"]

		try:
			while True:
				req = self.getMessage(sock)
				if req is None:
					break;
			
				print(addr + " >> " + req)

				#processazione della richiesta
				reqObj = json.loads(req)
				print(reqObj)

				try:
					reqObj["message"] = reqObj["message"]
					reqObj["error"] = reqObj["error"]
				except KeyError:
					print("Bad request")
					self.sendMessage(sock, "Bad request", 1)
					sock.close()
					break

				# crypto = base64.b64decode(reqObj["message"].encode("utf-8", "ignore"))
				# self.sendMessage(sock, rsa.decrypt(crypto, self.privateKey).decode("utf-8", "ignore"))
				# print(rsa.decrypt(crypto, self.privateKey).decode("utf-8", "ignore"))


				#da fare
				if (connectionState == ConnectionState["ACCEPTED"]):
					self.sendMessage(sock, "Ciao :)", 0)

				elif (connectionState == ConnectionState["KEYEXCHANGE"]):

					pass
			
			print(addr + " disconnected")
		
		except ConnectionResetError:
			print(addr + " disconnected")

		pass