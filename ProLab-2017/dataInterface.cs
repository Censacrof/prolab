﻿using System;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Windows;
using System.Xml.Serialization;

namespace ProLab_2017
{
    public struct dataSet
    {
        public string intestazione { get; set; }
        public string indirizzo { get; set; }
        public string partitaIva { get; set; }
        public string codiceFiscale { get; set; }
        public string licenseKey { get; set; }
        public string iban { get; set; }
        public string bancaAppoggio { get; set; }
        public string regMinSalute { get; set; }
        public string telefono { get; set; }
        public string regImprese { get; set; }
    }

    static class dataInterface
    {
        public static string dataFolderPath = "ProLab";
        public static string dataFilePath = "userData.xml";

        public static string backupFolderPath;

        public static bool isRegistered = false;
        public static string clientId = "";

        public static dataSet Data;
        
        public static void init()
        {
            //inizializzazione variabili
            dataFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\" + dataFolderPath;
            dataFilePath = dataFolderPath + @"\" + dataFilePath;

            //creo la cartella dei dati se non esite
            Directory.CreateDirectory(dataFolderPath);

            //se non esiste il file dei dati lo creo
            if (!File.Exists(dataFilePath))
                updateData(new dataSet());

            //creo la cartella dei backup
            backupFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ProLab\backup\";
            Directory.CreateDirectory(backupFolderPath);

            //client id
            string macAddress = (
                                    from nic in NetworkInterface.GetAllNetworkInterfaces()
                                    where nic.OperationalStatus == OperationalStatus.Up
                                    select nic.GetPhysicalAddress().ToString()
                                ).FirstOrDefault();
            clientId = activation.md5(macAddress);

            fetchData();
         }

        public static void fetchData()
        {
            //xml -> Data
            XmlSerializer xmls = new XmlSerializer(Data.GetType());
            StreamReader file = new StreamReader(dataFilePath);

            Data = (dataSet)xmls.Deserialize(file);

            file.Close();

            //licenza
            dataInterface.isRegistered = activation.isValidLicense(clientId, dataInterface.Data.licenseKey);
        }

        public static bool updateData(dataSet data)
        {
            try
            {
                Data = data;

                XmlSerializer xmls = new XmlSerializer(Data.GetType());
                StreamWriter file = new StreamWriter(dataFilePath);

                xmls.Serialize(file, Data);
                file.Close();

                fetchData();
                return true;
            }
            catch { fetchData(); return false; }

        }


        public static bool checkLicense()
        {
            return false;
        }
    }
}

