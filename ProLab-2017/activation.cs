﻿using System.Text;
using System.Security.Cryptography;
using System.Windows;

namespace ProLab_2017
{
    static class activation
    {
        public static string md5(string str)
        {
            MD5 _md5 = MD5.Create();

            byte[] strBytes = System.Text.Encoding.ASCII.GetBytes(str);
            byte[] hash = _md5.ComputeHash(strBytes);

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }

            return sb.ToString();
        }


        public static string ashFunction(string str)
        {
            return md5(md5(md5(str)));
        }
        
        public static bool isValidLicense(string partitaIva, string key)
        {
            if (partitaIva == null || key == null)
                return false;
            return ashFunction(partitaIva) == key;
        }
    }
}
