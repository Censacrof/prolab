﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProLab_2017
{
    /// <summary>
    /// Interaction logic for windowLavori.xaml
    /// </summary>
    public partial class windowLavori : Window
    {
        public windowLavori()
        {
            InitializeComponent();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            MainWindow.wLavoro = null;

            if (wScadenze != null)
                wScadenze.Close();

            wScadenze = null;
        }

        private bool isLavoroChiuso;
        public void refresh(bool refreshDropDown = true, bool clearForm = false, bool refreshData = true)
        {
            btnApriScadenze.IsEnabled = currentIDLavoro != null;
            btnApriBolla.IsEnabled = currentIDLavoro != null;
            btnApriProtocolli.IsEnabled = currentIDLavoro != null;
            btnSalvaLavoro.Content = currentIDLavoro == null ? "Inserisci Lavoro" : "Salva Modifiche";
            btnNuovoLavoro.Visibility = currentIDLavoro == null ? Visibility.Hidden : Visibility.Visible;
            chbChiuso.IsEnabled = currentIDLavoro != null; chbChiuso.IsChecked = false;
            chbConsegnato.IsEnabled = currentIDLavoro != null; chbConsegnato.IsChecked = false;


            dpcDataLavoro.IsEnabled = true;
            dpcDataPrescrizione.IsEnabled = true;
            tbxNomePaziente.IsEnabled = true;
            tbxCognomePaziente.IsEnabled = true;
            tbxAnnotazioni.IsEnabled = true;
            cmbCliente.IsEnabled = true;

            //refresh dropdown
            if (refreshDropDown)
            {
                cmbCliente.Items.Clear();

                SQLiteDataReader read = dbInterface.getClienteDataGrid();                
                while (read.Read())
                {
                    int id = int.Parse(read["ID"].ToString());
                    string name = read["NomeStudio"].ToString();

                    cmbCliente.Items.Add(new KeyValuePair<string, int>(name, id));
                }
            }
            
            if (clearForm)
            {
                dpcDataLavoro.SelectedDate = DateTime.Today;
                dpcDataLavoro.DisplayDate = DateTime.Today;

                dpcDataPrescrizione.SelectedDate = DateTime.Today;
                dpcDataPrescrizione.DisplayDate = DateTime.Today;

                tbxAnnotazioni.Text = "";
                tbxCognomePaziente.Text = "";
                tbxNomePaziente.Text = "";
            }

            //se currentIDLavoro != null metto la roba del db
            if (currentIDLavoro != null && refreshData)
            {
                SQLiteDataReader read = dbInterface.getLavoro((int)currentIDLavoro);

                while (read.Read())
                {
                    int chiuso = Convert.ToInt32(read["chiuso"]);
                    isLavoroChiuso = chiuso == 0 ? false : true;

                    utils.dpSetDateStringSQLITE(dpcDataLavoro, (string) read["dataLavoro"]); dpcDataLavoro.IsEnabled = !isLavoroChiuso;
                    utils.dpSetDateStringSQLITE(dpcDataPrescrizione, (string) read["dataPrescrizione"]); dpcDataPrescrizione.IsEnabled = !isLavoroChiuso;
                    tbxNomePaziente.Text = (string)read["nomePaziente"]; tbxNomePaziente.IsEnabled = !isLavoroChiuso;
                    tbxCognomePaziente.Text = (string)read["cognomePaziente"]; tbxCognomePaziente.IsEnabled = !isLavoroChiuso;
                    tbxAnnotazioni.Text = (string)read["annotazioni"]; tbxAnnotazioni.IsEnabled = !isLavoroChiuso;

                    long rightIdCliente = (long) read["idCliente"];

                    cmbCliente.SelectedIndex = -1;
                    foreach (KeyValuePair<string, int> item in cmbCliente.Items)
                    {
                        if (item.Value == rightIdCliente)
                        {
                            cmbCliente.SelectedIndex = cmbCliente.Items.IndexOf(item);
                            break;
                        }
                    }
                    cmbCliente.IsEnabled = !isLavoroChiuso;

                    chbChiuso.IsChecked = Convert.ToInt32(read["chiuso"]) == 1; chbChiuso.IsEnabled = !isLavoroChiuso;
                    chbConsegnato.IsChecked = Convert.ToInt32(read["consegnato"]) == 1;

                    btnApriScadenze.IsEnabled = !isLavoroChiuso;
                    btnApriBolla.IsEnabled = !isLavoroChiuso;
                    btnApriProtocolli.IsEnabled = !isLavoroChiuso;
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            refresh(true, true);
        }

        private void btnAggiungiCliente_Click(object sender, RoutedEventArgs e)
        {
            if (MainWindow.wCliente == null)
                MainWindow.wCliente = new windowCliente();

            MainWindow.wCliente.Show();
            MainWindow.wCliente.Focus();
        }


        public static Nullable<int> currentIDLavoro = null;
        private void btnSalvaLavoro_Click(object sender, RoutedEventArgs e)
        {
            if (!isLavoroChiuso && (bool)chbChiuso.IsChecked)
            {
                if (dbInterface.getCountBolla((int) currentIDLavoro) == 0)
                {
                    Dialog.MessageBox("Prima di chiudere il lavoro è necessario inserire qualcosa nella bolla");
                    chbChiuso.IsChecked = false;
                    return;
                }

                if (Dialog.MessageBox("Sei sicuro di voler chiudere il lavoro? Una volta chiuso un lavoro non può più essere modificato.\n RICONTROLLARE I DATI PRIMA DI CHIUDERE", "Prolab", System.Windows.MessageBoxButton.YesNo) == MessageBoxResult.No)
                    return;
            }
                

            if (currentIDLavoro == null)
            {

                //stiamo aggiungendo un record
                if (cmbCliente.SelectedIndex != -1 && dbInterface.insertLavoro(
                    utils.dpGetDateString(dpcDataLavoro),
                    utils.dpGetDateString(dpcDataPrescrizione),
                    tbxNomePaziente.Text,
                    tbxCognomePaziente.Text,
                    ((KeyValuePair<string, int>) cmbCliente.SelectedItem).Value,
                    tbxAnnotazioni.Text
                ))
                {
                    currentIDLavoro = (int) dbInterface.getLastInsertID();
                    Dialog.MessageBox("Lavoro Inserito");
                    
                    refresh(false);
                }
                else Dialog.MessageBox("Impossibile Inserire lavoro");
            }
            else
            {
                //stiamo modificando un record
                if (cmbCliente.SelectedIndex != -1 && dbInterface.updateLavoro(
                    (int) currentIDLavoro,
                    utils.dpGetDateString(dpcDataLavoro),
                    utils.dpGetDateString(dpcDataPrescrizione),
                    tbxNomePaziente.Text,
                    tbxCognomePaziente.Text,
                    ((KeyValuePair<string, int>)cmbCliente.SelectedItem).Value,
                    tbxAnnotazioni.Text,
                    (bool) chbChiuso.IsChecked ? 1 : 0,
                    (bool) chbConsegnato.IsChecked ? 1 : 0
                ))
                {
                    Dialog.MessageBox("Lavoro Modificato");

                    refresh(false);
                }
                else Dialog.MessageBox("Impossibile Modificare lavoro");
            }

            MainWindowInstanceProvider.mainWindow.refresh(true);
        }

        private void btnNuovoLavoro_Click(object sender, RoutedEventArgs e)
        {
            currentIDLavoro = null;
            refresh(true, true);

            if (wScadenze != null)
                wScadenze.Close();
            wScadenze = null;
        }

        public static lavoriPopUp.popupScadenze wScadenze = null;
        private void btnApriScadenze_Click(object sender, RoutedEventArgs e)
        {
            if (wScadenze == null)
                wScadenze = new lavoriPopUp.popupScadenze();

            wScadenze.ShowDialog();
        }

        public static lavoriPopUp.popupBolla wBolla = null;
        private void btnApriBolla_Click(object sender, RoutedEventArgs e)
        {
            if (wBolla == null)
                wBolla = new lavoriPopUp.popupBolla();

            wBolla.ShowDialog();
        }

        public static lavoriPopUp.popupProtocolli wProtocolli = null;
        private void btnApriProtocolli_Click(object sender, RoutedEventArgs e)
        {
            if (wProtocolli == null)
                wProtocolli = new lavoriPopUp.popupProtocolli();

            wProtocolli.ShowDialog();
        }
    }
}
