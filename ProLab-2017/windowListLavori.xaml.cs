﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProLab_2017
{
    /// <summary>
    /// Interaction logic for windowListLavori.xaml
    /// </summary>
    public partial class windowListLavori : Window
    {
        public windowListLavori()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            refresh();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            MainWindow.wListLavori = null;
        }

        private bool cmbMeseInitialized = false;
        private void cmbMeseInit()
        {
            cmbMese.Items.Clear();
            cmbMese.Items.Add(new KeyValuePair<string, int>("-- Tutti --", -1));
            cmbMese.Items.Add(new KeyValuePair<string, int>("Gennaio 01", 1));
            cmbMese.Items.Add(new KeyValuePair<string, int>("Febbraio 02", 2));
            cmbMese.Items.Add(new KeyValuePair<string, int>("Marzo 03", 3));
            cmbMese.Items.Add(new KeyValuePair<string, int>("Aprile 04", 4));
            cmbMese.Items.Add(new KeyValuePair<string, int>("Maggio 05", 5));
            cmbMese.Items.Add(new KeyValuePair<string, int>("Giugno 06", 6));
            cmbMese.Items.Add(new KeyValuePair<string, int>("Luglio 07", 7));
            cmbMese.Items.Add(new KeyValuePair<string, int>("Agosto 08", 8));
            cmbMese.Items.Add(new KeyValuePair<string, int>("Settembre 09", 9));
            cmbMese.Items.Add(new KeyValuePair<string, int>("Ottobre 10", 10));
            cmbMese.Items.Add(new KeyValuePair<string, int>("Novembre 11", 11));
            cmbMese.Items.Add(new KeyValuePair<string, int>("Dicembre 12", 12));

            cmbMese.SelectedIndex = 0;
        }

        private bool cmbAnnoInitialized = false;
        private void cmbAnnoInit()
        {
            cmbAnno.Items.Clear();
            cmbAnno.Items.Add(new KeyValuePair<string, int>("-- Tutti --", -1));

            int year = DateTime.Now.Year;
            for (int i = year; i >= year - 5; i--)
            {
                cmbAnno.Items.Add(new KeyValuePair<string, int>(i.ToString(), i));
            }

            cmbAnno.SelectedIndex = 0;
        }

        public void refresh(bool refreshDataGrid = true, bool refreshClienteDropDown = true)
        {
            if (!cmbMeseInitialized)
            {
                cmbMeseInit();
                cmbMeseInitialized = true;
            }

            if (!cmbAnnoInitialized)
            {
                cmbAnnoInit();
                cmbAnnoInitialized = true;
            }

            if (refreshClienteDropDown)
            {
                cmbCliente.Items.Clear();
                cmbCliente.Items.Add(new KeyValuePair<string, int>("-- Tutti --", -1));

                SQLiteDataReader read = dbInterface.getClienteDataGrid();
                while (read.Read())
                {
                    int id = int.Parse(read["ID"].ToString());
                    string name = read["NomeStudio"].ToString();

                    cmbCliente.Items.Add(new KeyValuePair<string, int>(name, id));
                }

                cmbCliente.SelectedIndex = 0;
            }

            if (refreshDataGrid)
                dtgListLavoriRefresh();
        }

        DataTable dataTableListLavori;
        private void dtgListLavoriRefresh()
        {
            dataTableListLavori = new DataTable();
            SQLiteDataReader dr = dbInterface.getListLavori(((KeyValuePair<string, int>)cmbCliente.SelectedItem).Value,
                ((KeyValuePair<string, int>)cmbMese.SelectedItem).Value,
                ((KeyValuePair<string, int>)cmbAnno.SelectedItem).Value,
                (bool) chbChiuso.IsChecked,
                (bool) chbConsegnato.IsChecked);
            dataTableListLavori.Load(dr);
            dtgListLavori.DataContext = dataTableListLavori.DefaultView;
        }

        private void btnModificaLavoro_Click(object sender, RoutedEventArgs e)
        {
            if (dtgListLavori.SelectedItems.Count <= 0)
            {
                Dialog.MessageBox("Nessun elemento selezionato");
                return;
            } else if (dtgListLavori.SelectedItems.Count > 1)
            {
                Dialog.MessageBox("Selezionare un solo lavoro da modificare");
                return;
            }

            foreach (DataRowView row in dtgListLavori.SelectedItems)
            {
                if (MainWindow.wLavoro == null) MainWindow.wLavoro = new windowLavori();
                windowLavori.currentIDLavoro = (int.Parse(row["ID"].ToString()));

                MainWindow.wLavoro.refresh(true, true, true);
                MainWindow.wLavoro.Show();
                MainWindow.wLavoro.Focus();
            }
                
        }

        private void btnEliminaLavoro_Click(object sender, RoutedEventArgs e)
        {
            if (dtgListLavori.SelectedItems.Count == 0)
            {
                Dialog.MessageBox("Nessun elemento selezionato");
                return;
            }

            if (Dialog.MessageBox("Sei sicuro di voler rimuovere " + dtgListLavori.SelectedItems.Count + " elemento/i?", "Conferma eliminazione", MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                return;

            foreach (DataRowView row in dtgListLavori.SelectedItems)
                dbInterface.removeLavoro(int.Parse(row["ID"].ToString()));

            dtgListLavoriRefresh();

            MainWindowInstanceProvider.mainWindow.refresh();
        }

        private void btnCerca_Click(object sender, RoutedEventArgs e)
        {
            refresh(true, false);
        }
    }
}
