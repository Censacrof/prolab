﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProLab_2017
{
    /// <summary>
    /// Logica di interazione per windowLaboratorio.xaml
    /// </summary>
    public partial class windowLaboratorio : Window
    {
        public windowLaboratorio()
        {
            InitializeComponent();
        }

        private void tbxPartitaIva_TextChanged(object sender, TextChangedEventArgs e)
        {
            tbxPartitaIva.Text = Regex.Replace(tbxPartitaIva.Text, @"[^0-9]", "");
            tbxPartitaIva.SelectionStart = tbxPartitaIva.Text.Length;
            tbxPartitaIva.SelectionLength = 0;
        }

        private void refreshData()
        {
            dataInterface.fetchData();
            txbClientId.Text = dataInterface.clientId;

            tbxCodFiscale.Text = dataInterface.Data.codiceFiscale;
            tbxIntestazione.Text = dataInterface.Data.intestazione;
            tbxIndirizzo.Text = dataInterface.Data.indirizzo;
            tbxBancaAppoggio.Text = dataInterface.Data.bancaAppoggio;
            tbxKey.Text = dataInterface.Data.licenseKey;
            tbxPartitaIva.Text = dataInterface.Data.partitaIva;
            tbxRegistroImprese.Text = dataInterface.Data.regImprese;
            tbxRegMinSalute.Text = dataInterface.Data.regMinSalute;
            tbxTelefono.Text = dataInterface.Data.telefono;
            tbxIban.Text = dataInterface.Data.iban;

            tbxKey.IsEnabled = !dataInterface.isRegistered;
            lblIsAttivo.Content = dataInterface.isRegistered ? "Prolab è attivato" : "Prolab non è attivo";
            lblIsAttivo.Foreground = dataInterface.isRegistered ? new SolidColorBrush(Color.FromRgb(63, 181, 84)) : new SolidColorBrush(Color.FromRgb(218, 76, 101));

            utils.MainWindowInstance.refresh();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            refreshData();
        }

        private void btnSalva_Click(object sender, RoutedEventArgs e)
        {
            dataInterface.updateData(new dataSet()
            {
                codiceFiscale = tbxCodFiscale.Text,
                intestazione = tbxIntestazione.Text,
                indirizzo = tbxIndirizzo.Text,
                bancaAppoggio = tbxBancaAppoggio.Text,
                licenseKey = tbxKey.Text,
                partitaIva = tbxPartitaIva.Text,
                regImprese = tbxRegistroImprese.Text,
                regMinSalute = tbxRegMinSalute.Text,
                telefono = tbxTelefono.Text,
                iban = tbxIban.Text
            });

            refreshData();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            MainWindow.wLaboratorio = null;
        }

        private void btnCopiaCodUtente_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(txbClientId.Text);
        }
    }
}
