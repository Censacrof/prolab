﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;   
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProLab_2017
{
    public struct DatiFatturaItem
    {
        public string descrizione;
        public int quantita;
        public double prezzoUni;
        public double importo;
    }

    public struct DatiFattura
    {
        public string sNomeStudio, sIndirizzo, sPartitaIva, sIban, sBancaAppoggio, sTel, sCodFis, sRegMin, sRegImp, sNumeroFattura, sDataFattura;
        public string cNomeStudio, cIndirizzo;
        public string modPag, studio;
        public double marcaBollo1, marcaBollo2, marcaBollo3, totNetto;
        public List<DatiFatturaItem> items;
    }

    public struct Dati9342Item
    {
        public string fascicolo, operatore, descrizione;
    }

    public struct Dati9342
    {
        public string sNomeStudio, sIndirizzo, sNumeroFattura, sDataFattura;
        public string cNomeStudio, cIndirizzo;
        public List<Dati9342Item> items;
    }

    /// <summary>
    /// Interaction logic for windowListFattura.xaml
    /// </summary>
    public partial class windowListFattura : Window
    {
        public windowListFattura()
        {
            InitializeComponent();

            webFattura.LoadCompleted += new System.Windows.Navigation.LoadCompletedEventHandler(webFatturaPageLoaded);
        }        

        private void Window_Closed(object sender, EventArgs e)
        {
            MainWindow.wListFattura = null;
        }

        bool webPageLoaded = false;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cmbAnnoInit();
            refresh();
            dtgFattureRefresh();
            isFascicolo = false;
        }

        private void webFatturaPageLoaded(object sender, EventArgs e)
        {
            webPageLoaded = true;

            if (isFascicolo)
            {
                //visualizza fascicolo
                foreach (DataRowView row in dtgFatture.SelectedItems)
                {
                    Dati9342 dati = new Dati9342();

                    dati.sNomeStudio = dataInterface.Data.intestazione;
                    dati.sNumeroFattura = Convert.ToString(row["# Fattura"]);
                    dati.sDataFattura = Convert.ToString(row["Data"]);

                    dbInterface.fascicoloFillMissingFascicoloDati(ref dati, Convert.ToInt32(row["# Fattura"]));

                    string json = JsonConvert.SerializeObject(dati);
                    try { webFattura.InvokeScript("setData", json); } catch (Exception Ex) { utils.DebugMessage(Ex.ToString()); }

                    Console.WriteLine(json);
                }
            }
            else
            {
                //visualizza fattura
                foreach (DataRowView row in dtgFatture.SelectedItems)
                {
                    DatiFattura dati = new DatiFattura();

                    dati.sNomeStudio = dataInterface.Data.intestazione;
                    dati.sIndirizzo = dataInterface.Data.indirizzo;
                    dati.sPartitaIva = dataInterface.Data.partitaIva;
                    dati.sTel = dataInterface.Data.telefono;
                    dati.sCodFis = dataInterface.Data.codiceFiscale;
                    dati.sRegMin = dataInterface.Data.regMinSalute;
                    dati.sRegImp = dataInterface.Data.regImprese;
                    dati.sIban = dataInterface.Data.iban;
                    dati.sBancaAppoggio = dataInterface.Data.bancaAppoggio;

                    dati.sNumeroFattura = Convert.ToString(row["# Fattura"]);
                    dati.sDataFattura = Convert.ToString(row["Data"]);

                    dbInterface.fatturaFillMissingFatturaDati(ref dati, Convert.ToInt32(row["# Fattura"]));

                    string json = JsonConvert.SerializeObject(dati);
                    try { webFattura.InvokeScript("setData", json); } catch (Exception Ex) { utils.DebugMessage(Ex.ToString()); }

                    Console.WriteLine(json);
                }
            }
        }

        private void cmbAnnoInit()
        {
            cmbAnno.Items.Clear();
            cmbAnno.Items.Add(new KeyValuePair<string, int>("-- Tutti --", -1));

            int year = DateTime.Now.Year;
            for (int i = year; i >= year - 5; i--)
            {
                cmbAnno.Items.Add(new KeyValuePair<string, int>(i.ToString(), i));
            }

            cmbAnno.SelectedIndex = 0;
        }

        DataTable dataTableFatture;
        private void dtgFattureRefresh()
        {
            int idCliente = -1;
            int anno = -1;

            try { KeyValuePair<string, int> selCliente = (KeyValuePair<string, int>)cmbCliente.SelectedItem; idCliente = selCliente.Value; } catch { utils.DebugMessage("asd"); }
            try { KeyValuePair<string, int> selAnno = (KeyValuePair<string, int>)cmbAnno.SelectedItem; anno = selAnno.Value; } catch { utils.DebugMessage("aro"); }

            dataTableFatture = new DataTable();
            SQLiteDataReader dr = dbInterface.getFattura(idCliente, anno);
            dataTableFatture.Load(dr);
            dtgFatture.DataContext = dataTableFatture.DefaultView;
        }

        private void refresh()
        {
            //refresh dropdown
            cmbCliente.Items.Clear();
            cmbCliente.Items.Add(new KeyValuePair<string, int>("-- Tutti i clienti --", -1));

            SQLiteDataReader read = dbInterface.getClienteDataGrid();
            while (read.Read())
            {
                int id = int.Parse(read["ID"].ToString());
                string name = read["NomeStudio"].ToString();

                cmbCliente.Items.Add(new KeyValuePair<string, int>(name, id));
            }

            cmbCliente.SelectedIndex = 0;
        }

        private void cmbCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (webPageLoaded)
                dtgFattureRefresh();
        }

        private void cmbAnno_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (webPageLoaded)
                dtgFattureRefresh();
        }

        private void dtgFatture_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtgFatture.SelectedItems.Count > 1)
            {
                Dialog.MessageBox("Selezionare un solo lavoro da modificare");
                return;
            }

            isFascicolo = false;                        
        }

        private bool ___isFascicolo;
        private bool isFascicolo {
            get {
                return ___isFascicolo;
            }
            set {
                ___isFascicolo = value;
                btnCaricaFascicolo.Content = value ? "Carica Fattura" : "Carica 93/42";
                btnCaricaFascicolo.IsEnabled = dtgFatture.SelectedIndex != -1 && dtgFatture.SelectedItems.Count == 1;

                webPageLoaded = false;
                if (value)
                    webFattura.NavigateToString(File.ReadAllText("fascicolo.html"));
                else
                    webFattura.NavigateToString(File.ReadAllText("fattura.html"));
            }
        }

        private void btnCaricaFascicolo_Click(object sender, RoutedEventArgs e)
        {
            if (dtgFatture.SelectedItems.Count > 1)
                return;

            isFascicolo = !isFascicolo;
        }

        private void btnStampa_Click(object sender, RoutedEventArgs e)
        {
            mshtml.IHTMLDocument2 doc = webFattura.Document as mshtml.IHTMLDocument2;
            doc.execCommand("Print", true, null);
        }
    }
}
