﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProLab_2017
{
    /// <summary>
    /// Logica di interazione per windowCliente.xaml
    /// </summary>
    public partial class windowCliente : Window
    {
        public windowCliente()
        {
            InitializeComponent();
        }

        DataTable dataTableCliente;
        private void dtgClienteRefresh()
        {
            dataTableCliente = new DataTable();
            SQLiteDataReader dr = dbInterface.getClienteDataGrid();
            dataTableCliente.Load(dr);
            dtgCliente.DataContext = dataTableCliente.DefaultView;

            tbxBancaAppoggio.Text = tbxCodFiscale.Text = tbxIban.Text = tbxIndirizzo.Text = tbxMail.Text = tbxNomeStudio.Text = tbxPartitaIva.Text = tbxTelefono.Text = tbxTelefono.Text = "";
        }

        private void btnInserisci_Click(object sender, RoutedEventArgs e)
        {
            if (dbInterface.insertCliente(tbxNomeStudio.Text, tbxIndirizzo.Text, tbxPartitaIva.Text, tbxCodFiscale.Text, tbxTelefono.Text, tbxIban.Text, tbxBancaAppoggio.Text, tbxMail.Text))
            {
                Dialog.MessageBox("Cliente inserito con successo!");
                dtgClienteRefresh();

                //se è aperta la finestra lavoro la refresho
                if (MainWindow.wLavoro != null)
                {
                    MainWindow.wLavoro.refresh();
                }
            }
            else
                Dialog.MessageBox("Impossibile insererire cliente");            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            dtgClienteRefresh();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            MainWindow.wCliente = null;
        }

        private void btnRimuovi_Click(object sender, RoutedEventArgs e)
        {
            if (dtgCliente.SelectedItems.Count == 0)
            {
                Dialog.MessageBox("Nessun elemento selezionato");
                return;
            }

            if (Dialog.MessageBox("Sei sicuro di voler rimuovere " + dtgCliente.SelectedItems.Count + " elemento/i?", "Conferma eliminazione", MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                return;

            foreach (DataRowView row in dtgCliente.SelectedItems)
                dbInterface.removeCliente(int.Parse(row["ID"].ToString()));

            dtgClienteRefresh();

            //se è aperta la finestra lavoro la refresho
            if (MainWindow.wLavoro != null)
            {
                MainWindow.wLavoro.refresh();
            }
        }
    }
}
