﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace ProLab_2017
{
    static class utils
    {
        public const string DATE_IT_FORMAT = "dd/MM/yyyy";
        public const string DATE_SQLITE_FORMAT = "yyyy-MM-dd";
        public const string DATE_NULL_VALUE = "01/01/1970";

        public static MainWindow MainWindowInstance;

        [Conditional("DEBUG")]
        public static void DebugMessage(string message, string title = "Debug Message")
        {
            Dialog.MessageBox(message, title);
        }

        public static string validateNumberTxb(string str)
        {
            string s = Regex.Replace(str, @"[^0-9.,]", "");
            s = s.Replace(',', '.');
            int n = s.IndexOf('.');
            if (n != -1)
                s = string.Concat(s.Substring(0, n + 1), s.Substring(n + 1).Replace(".", ""));
            if (n == 0)
                s = "0" + s;

            return s;
        }

        public static void numericTextBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = (TextBox) sender;
            tb.Text = utils.validateNumberTxb(tb.Text);
            tb.SelectionStart = tb.Text.Length == 0 ? 0 : tb.Text.Length;
            tb.SelectionLength = 0;
        }

        public static void numericTextBoxLostFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox) sender;
            if (tb.Text.Length > 0)
            {
                if (tb.Text[tb.Text.Length - 1] == '.')
                    tb.Text = tb.Text.Replace(".", "");
            } else
                tb.Text = "0.00";
        }

        public static void populateCmbOrario(ComboBox cmb, int start = 7, int end = 21)
        {
            cmb.DisplayMemberPath = "Key";
            cmb.SelectedValuePath = "Value";


            for (int i = start; i < end; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    string s;
                    if (j % 2 == 0)
                    {
                        s = new DateTime(61, 1, 1, i, 0, 0).ToString("HH:mm");
                    }
                        
                    else
                    {
                        s = new DateTime(61, 1, 1, i, 30, 0).ToString("HH:mm");
                    }

                    cmb.Items.Add(new KeyValuePair<string, string>(s, s));
                }                
            }
        }

        public static void populateCmbDispositivo(ComboBox cmb)
        {
            cmb.DisplayMemberPath = "Value";
            cmb.SelectedValuePath = "Key";

            cmb.Items.Add(new KeyValuePair<int, string>(1, "Protesi Fissa"));
            cmb.Items.Add(new KeyValuePair<int, string>(2, "Protesi Mobile"));
            cmb.Items.Add(new KeyValuePair<int, string>(3, "Protesi Combinata"));
            cmb.Items.Add(new KeyValuePair<int, string>(4, "Protesi Scheletrica"));
            cmb.Items.Add(new KeyValuePair<int, string>(5, "Protesi Provvisoria"));
            cmb.Items.Add(new KeyValuePair<int, string>(6, "Protesi Ortodontica"));
        }

        public static void populateCmbOperatore(ComboBox cmb)
        {
            cmb.DisplayMemberPath = "Value";
            cmb.SelectedValuePath = "Key";

            cmb.Items.Add(new KeyValuePair<int, string>(1, "Lavorazione Interna"));
            cmb.Items.Add(new KeyValuePair<int, string>(2, "Cliente Professionista"));
            cmb.Items.Add(new KeyValuePair<int, string>(3, "Lavorazione Esterna"));

            cmb.SelectedIndex = 0;
        }

        public static string dpGetDateString(DatePicker dp)
        {
            if (dp.SelectedDate == null) return null;
            else return dp.SelectedDate.Value.ToString(DATE_SQLITE_FORMAT);
        }

        public static void dpSetDateStringIT(DatePicker dp, string date)
        {
            try
            {
                DateTime myDate = DateTime.ParseExact(date, DATE_IT_FORMAT, System.Globalization.CultureInfo.InvariantCulture);

                dp.SelectedDate = myDate;
            } catch (Exception e)
            {
                DebugMessage(e.ToString());
                dp.SelectedDate = null;
            }
            
        }

        public static void dpSetDateStringSQLITE(DatePicker dp, string date)
        {
            try
            {
                DateTime myDate = DateTime.ParseExact(date, DATE_SQLITE_FORMAT, System.Globalization.CultureInfo.InvariantCulture);

                dp.SelectedDate = myDate;
            } catch (Exception e)
            {
                DebugMessage(e.ToString());
                dp.SelectedDate = null;
            }          
        }


        public static bool isFieldNull(SQLiteDataReader read, string field)
        {
            return read.IsDBNull(read.GetOrdinal(field));
        }


    }
}
