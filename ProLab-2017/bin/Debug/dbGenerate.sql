﻿create table listino 
(
	ID integer not null primary key autoincrement,
	descrizione varchar(250) not null,
	prezzo double not null /*prezzo in centesimi*/
);


create table cliente
(
	ID integer not null primary key autoincrement,
	nomeStudio varchar(100) not null,
	indirizzo varchar(100) not null,
	partitaIva char(11) not null unique,
	codFiscale char(16) null unique,
	telefono varchar(20) null,
	iban varchar(40) null unique,
	bancaAppoggio varchar(80) null,
	email varchar(255) null
);

/* date: dataLavoro, prescrizione, ritiro, varieProve, consegna */
create table lavoro
(
	ID integer not null primary key autoincrement,
	dataLavoro char(10) not null,
	dataPrescrizione char(10) null default null,
	dataRitiro char(19) null default null,
	dataConsegna char(10) null default null,

	cognomePaziente varchar(50) not null,
	nomePaziente varchar(50) not null,
	annotazioni varchar(200) null,

	idCliente integer not null,

	foreign key(idCliente) references cliente(ID) on update cascade on delete cascade
);


create table dataProva
(
	ID integer not null primary key autoincrement,
	idLavoro integer not null,
	data char(19) not null,
	ora char(5) not null,
	annotazioni varchar(100) null,

	foreign key(idLavoro) references lavoro(ID) on update cascade on delete cascade
);



create table bolla
(
	ID integer not null primary key autoincrement,
	quantita int not null default 1,
	idLavoro int not null,
	idListino int not null,

	foreign key(idLavoro) references lavoro(ID) on update cascade on delete cascade,
	foreign key(idListino) references listino(ID) on update cascade on delete cascade
);