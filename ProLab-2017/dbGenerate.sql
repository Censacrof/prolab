﻿begin transaction;

	/* Variabili */
	create table dbVar
	(
		var varchar(25) not null primary key,
		value varchar(255) null,

		unique(var) on conflict replace
	);
	insert into dbVar(var, value) values("dbVersion", "1.0");

	create table listino 
	(
		ID integer not null primary key autoincrement,
		descrizione varchar(250) not null,
		prezzo double not null
	);


	create table cliente
	(
		ID integer not null primary key autoincrement,
		nomeStudio varchar(100) not null,
		indirizzo varchar(100) not null,
		partitaIva char(11) not null unique,
		codFiscale char(16) null unique,
		telefono varchar(20) null,
		iban varchar(40) null unique,
		bancaAppoggio varchar(80) null,
		email varchar(255) null
	);

	/* date: dataLavoro, prescrizione, ritiro, varieProve, consegna */
	create table lavoro
	(
		ID integer not null primary key autoincrement,
		dataLavoro char(10) not null,
		dataPrescrizione char(10) null default null,
		dataRitiro char(19) null default null,
		dataConsegna char(19) null default null,

		cognomePaziente varchar(50) not null,
		nomePaziente varchar(50) not null,
		annotazioni varchar(200) null,
		chiuso tinyint(1) default 0,
		consegnato tinyint(1) default 0,

		ultimoAccesso varchar(19) null,

		idCliente integer not null,

		foreign key(idCliente) references cliente(ID) on update cascade on delete cascade
	);

	create trigger trgAInsertLavoro 
		after insert on lavoro
		begin 
			update lavoro set ultimoAccesso = datetime('now', 'localtime')
			where ID = new.ID;
		end;
	create trigger trgAUpdateLavoro 
		after update on lavoro
		begin 
			update lavoro set ultimoAccesso = datetime('now', 'localtime')
			where ID = new.ID;
		end;

	create table fascicolo	
	(
		ID integer not null primary key autoincrement,
		nome varchar(255) not null,
		dispositivo varchar(100) not null
	);

	create table descFascicolo	
	(
		ID integer not null primary key autoincrement,
		ordine integer not null,
		operatore varchar(100) not null,
		descrizione varchar(255) not null,
		idFascicolo integer not null,

		foreign key(idFascicolo) references fascicolo(ID) on update cascade on delete cascade
	);

	create table fascicoliLavori
	(
		idLavoro integer not null,
		idFascicolo integer not null,

		primary key(idLavoro, idFascicolo),
		foreign key(idLavoro) references lavoro(ID) on update cascade on delete cascade,
		foreign key(idFascicolo) references fascicolo(ID) on update cascade on delete cascade
	);

	create table dataProva
	(
		ID integer not null primary key autoincrement,
		idLavoro integer not null,
		data char(19) not null,
		ora char(5) not null,
		annotazioni varchar(100) null,

		foreign key(idLavoro) references lavoro(ID) on update cascade on delete cascade
	);

	create trigger trgAInsertDataProva
		after insert on dataProva
		begin
			update lavoro set ultimoAccesso = datetime('now', 'localtime')
			where ID = new.idLavoro;
		end;
	create trigger trgAUpdateDataProva
		after insert on dataProva
		begin
			update lavoro set ultimoAccesso = datetime('now', 'localtime')
			where ID = new.idLavoro;
		end;

	create view vDataProvaSettimana as
	select d.data, d.ora, d.annotazioni, ((cast(strftime('%w', d.data) as integer) -1) % 7) as giornoSettimana, c.nomeStudio, l.cognomePaziente || " " || l.nomePaziente as paziente
	from dataProva d
	join lavoro l
		on d.idLavoro = l.ID
	join cliente c
		on l.idCliente = c.ID
	where date(d.data) 
		between date('now', '-' || ((cast(strftime('%w') as integer) - 1) % 7) || ' day') 
		and date('now', '+' || (6 - ((cast(strftime('%w') as integer) - 1) % 7)) || ' day')
	union 
	select date(l.dataConsegna), strftime("%H:%M", l.dataConsegna), "Consegna lavoro", ((cast(strftime('%w', l.dataConsegna) as integer) -1) % 7) as giornoSettimana, c.nomeStudio, l.cognomePaziente || " " || l.nomePaziente as paziente
	from lavoro l
	join cliente c
		on l.idCliente = c.ID
	where date(l.dataConsegna) 
		between date('now', '-' || ((cast(strftime('%w') as integer) - 1) % 7) || ' day') 
		and date('now', '+' || (6 - ((cast(strftime('%w') as integer) - 1) % 7)) || ' day');


	create view vDataProvaOggi as
	select ID as idLavoro, strftime('%Y-%m-%d', dataConsegna) as Data, strftime('%H:%M', dataConsegna) as Ora, '-- Consegna --' as Annotazioni
	from lavoro
	where dataConsegna is not null
	union
	select idLavoro, strftime('%Y-%m-%d', data) as Data, ora as Ora, annotazioni as Annotazioni
	from dataProva;

	create table fattura
	(
		ID integer not null primary key autoincrement,
		data char(10) not null,
		modPagamento varchar(50) not null,
		marcaBollo1 int not null default 0.0,
		marcaBollo2 int not null default 0.0,
		marcaBollo3 int not null default 0.0
	);

	create table bolla
	(
		ID integer not null primary key autoincrement,
		quantita int not null default 1,
		prezzoUnitario double not null,
		idLavoro int not null,
		idListino int not null,
		idFattura int null,

		foreign key(idLavoro) references lavoro(ID) on update cascade on delete cascade,
		foreign key(idListino) references listino(ID) on update cascade on delete cascade,
		foreign key(idFattura) references fattura(ID) on update cascade on delete set null
	);

	create trigger trgAInsertBolla
		after insert on bolla
		begin
			update lavoro set ultimoAccesso = datetime('now', 'localtime')
			where ID = new.idLavoro;
		end;
	create trigger trgAUpdateBolla
		after insert on bolla
		begin
			update lavoro set ultimoAccesso = datetime('now', 'localtime')
			where ID = new.idLavoro;
		end;

commit;