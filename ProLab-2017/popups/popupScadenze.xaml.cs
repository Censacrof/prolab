﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProLab_2017.lavoriPopUp
{
    /// <summary>
    /// Interaction logic for popupScadenze.xaml
    /// </summary>
    public partial class popupScadenze : Window
    {
        public popupScadenze()
        {
            InitializeComponent();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            windowLavori.wScadenze = null;
        }

        DataTable dataTableProve;
        private void dtgProveRefresh()
        {
            dataTableProve = new DataTable();
            SQLiteDataReader dr = dbInterface.getProve((int)windowLavori.currentIDLavoro);
            dataTableProve.Load(dr);
            dtgProve.DataContext = dataTableProve.DefaultView;
        }

        public void refresh()
        {
            //pulisco le textbox
            cmbOra.SelectedIndex = 0;
            
            txtbAnnotazioni.Text = "";
            dpcData.SelectedDate = null;

            //se non c'è nessun lavoro corrispondente chiudo il popup
            SQLiteDataReader read = dbInterface.getLavoro((int)windowLavori.currentIDLavoro);
            if (read != null && !read.Read())
            {
                this.Close();
                return;
            }

            if (!utils.isFieldNull(read, "dataRitiro")) utils.dpSetDateStringSQLITE(dpcDataRitiro, (string)read["dataRitiro"]);
            if (!utils.isFieldNull(read, "dataConsegna")) utils.dpSetDateStringSQLITE(dpcDataConsegna, (string)read["dataConsegna"]);

            if (!utils.isFieldNull(read, "oraConsegna"))
            {
                string oraConsegna = (string)read["oraConsegna"];

                foreach (KeyValuePair<string, string> item in cmbOraConsegna.Items)
                {
                    if (item.Value == oraConsegna)
                    {
                        cmbOraConsegna.SelectedIndex = cmbOraConsegna.Items.IndexOf(item);
                        break;
                    }
                }
            }

            dtgProveRefresh();
        }

        private void Window_LostFocus(object sender, RoutedEventArgs e)
        {
            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            utils.populateCmbOrario(cmbOra);
            utils.populateCmbOrario(cmbOraConsegna);
            refresh();
        }

        private void btnSalva_Click(object sender, RoutedEventArgs e)
        {
            if (dbInterface.updateLavoroScadenze((int)windowLavori.currentIDLavoro, utils.dpGetDateString(dpcDataRitiro), utils.dpGetDateString(dpcDataConsegna) + " " + (string)cmbOraConsegna.SelectedValue + ":00"))
                Dialog.MessageBox("Lavoro aggiornato con successo");
            else
                Dialog.MessageBox("Impossibile aggiornare lavoro");
            refresh();
        }

        private void btnInserisciProva_Click(object sender, RoutedEventArgs e)
        {
            if (dbInterface.insertProva((int)windowLavori.currentIDLavoro, utils.dpGetDateString(dpcData), (string)cmbOra.SelectedValue, txtbAnnotazioni.Text != "" ? txtbAnnotazioni.Text : null))
            {
                Dialog.MessageBox("Data di Prova inserita con successo");
                refresh();
            }
            else Dialog.MessageBox("Impossibile inserire data di prova");            
        }

        private void btnRimuoviProva_Click(object sender, RoutedEventArgs e)
        {
            if (dtgProve.SelectedItems.Count == 0)
            {
                Dialog.MessageBox("Nessun elemento selezionato");
                return;
            }

            if (Dialog.MessageBox("Sei sicuro di voler rimuovere " + dtgProve.SelectedItems.Count + " elemento/i?", "Conferma eliminazione", MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                return;

            foreach (DataRowView row in dtgProve.SelectedItems)
                dbInterface.removeProva(int.Parse(row["ID"].ToString()));

            dtgProveRefresh();
        }
    }
}
