﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProLab_2017.lavoriPopUp
{
    /// <summary>
    /// Interaction logic for popupProtocolli.xaml
    /// </summary>
    public partial class popupProtocolli : Window
    {
        public popupProtocolli()
        {
            InitializeComponent();

            utils.populateCmbDispositivo(cmbDispositivo);

            cmbFascicolo.DisplayMemberPath = "Value";
            cmbFascicolo.SelectedValuePath = "Key";
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            windowLavori.wProtocolli = null;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            refresh();            
        }

        private void refresh(bool refreshDtgFascicolo = true, bool refreshCmb = true)
        {
            if (refreshCmb)
            {
                //triggero l'evento "Selection Changed" di cmbDispositivo
                int lastSelected = cmbDispositivo.SelectedIndex;
                cmbDispositivo.SelectedIndex = -1;
                cmbDispositivo.SelectedIndex = lastSelected != -1 ? lastSelected : 0;
            }
            
            if (refreshDtgFascicolo)
                dtgFascicoliRefresh();
        }

        DataTable dataTableFascicoli;
        private void dtgFascicoliRefresh()
        {
            dataTableFascicoli = new DataTable();
            SQLiteDataReader dr = dbInterface.getFascicoliLavoro((int)windowLavori.currentIDLavoro);
            dataTableFascicoli.Load(dr);
            dtgFascicoli.DataContext = dataTableFascicoli.DefaultView;            
        }

        private void cmbDispositivo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbDispositivo.SelectedIndex == -1)
                return;

            KeyValuePair<int, string> dispSel = (KeyValuePair<int, string>)cmbDispositivo.SelectedItem;
            SQLiteDataReader read = dbInterface.getFascicolo(dispSel.Value);

            //setto cmbFascicolo
            cmbFascicolo.Items.Clear();

            while (read.Read())
            {
                cmbFascicolo.Items.Add(new KeyValuePair<int, string>(Convert.ToInt32(read["ID"]), Convert.ToString(read["nome"])));
            }

            cmbFascicolo.SelectedIndex = 0;
        }

        private void btnInserisci_Click(object sender, RoutedEventArgs e)
        {
            if (cmbFascicolo.SelectedIndex == -1)
            {
                Dialog.MessageBox("Selezionare prima un fascicolo");
                return;
            }

            KeyValuePair<int, string> selFasc = (KeyValuePair<int, string>) cmbFascicolo.SelectedItem;
            try
            {
                if (dbInterface.insertFascicoloLavoro(selFasc.Key, (int)windowLavori.currentIDLavoro))
                {
                    Dialog.MessageBox("Fascicolo assegnato con successo");
                    refresh(true, false);
                }
                else Dialog.MessageBox("Impossibile assegnare fascicolo");
            }
            catch (SQLiteException Ex)
            {
                if (Ex.ErrorCode == SQLiteErrorCode.Constraint)
                    Dialog.MessageBox("Il fascicolo selezionato risulta gia assegnato al lavoro corrente");
                else Dialog.MessageBox("Impossibile assegnare fascicolo");
            }
            catch (Exception Ex) { utils.DebugMessage(Ex.ToString()); Dialog.MessageBox("Impossibile assegnare fascicolo"); }
        }

        private void btnRimuovi_Click(object sender, RoutedEventArgs e)
        {
            if (dtgFascicoli.SelectedItems.Count == 0)
            {
                Dialog.MessageBox("Nessun elemento selezionato");
                return;
            }

            if (Dialog.MessageBox("Sei sicuro di voler rimuovere " + dtgFascicoli.SelectedItems.Count + " lavorazione/i dal lavoro?", "Conferma rimozione", MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                return;

            foreach (DataRowView row in dtgFascicoli.SelectedItems)
                dbInterface.removeFascicoloLavoro(Convert.ToInt32(row["ID"]), (int)windowLavori.currentIDLavoro);

            refresh(true, false);
        }
    }
}
