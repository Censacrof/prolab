﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProLab_2017.lavoriPopUp
{
    /// <summary>
    /// Interaction logic for popupBolla.xaml
    /// </summary>
    public partial class popupBolla : Window
    {
        public popupBolla()
        {
            InitializeComponent();

            //do comportamento da numeric textbox a tbxPrezzoUnitario
            tbxPrezzoUnitario.TextChanged += utils.numericTextBoxTextChanged;
            tbxPrezzoUnitario.LostFocus += utils.numericTextBoxLostFocus;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            windowLavori.wBolla = null;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            refresh();
        }

        DataTable dataTableBolla;
        private void dtgBollaRefresh()
        {
            dataTableBolla = new DataTable();
            SQLiteDataReader dr = dbInterface.getBolla(windowLavori.currentIDLavoro);
            dataTableBolla.Load(dr);
            dtgBolla.DataContext = dataTableBolla.DefaultView;

            double tot = 0;
            foreach (DataRow row in dataTableBolla.Rows)
            {
                tot += int.Parse(row["quantita"].ToString()) * double.Parse(row["prezzoUnitario"].ToString());
            }

            tbxTotale.Text = tot.ToString();
        }

        bool cmbQuantitaInitialized = false;
        public void refresh()
        {
            if (!cmbQuantitaInitialized)
            {
                cmbQuantita.Items.Clear();

                for (int i = 1; i <= 50; i++)
                    cmbQuantita.Items.Add(i);

                cmbQuantitaInitialized = true;
            }
            cmbQuantita.SelectedIndex = 0;

            //cmbListino
            cmbListino.Items.Clear();
            SQLiteDataReader read = dbInterface.getListino();
            while (read.Read())
            {
                int id = int.Parse(read["ID"].ToString());
                string name = read["descrizione"].ToString();

                cmbListino.Items.Add(new KeyValuePair<string, int>(name, id));
            }
            cmbListino.SelectedIndex = 0;
            //tbxPrezzoUnitario.Text = dbInterface.getListinoPrezzo(((KeyValuePair<string, int>)cmbListino.SelectedItem).Value).ToString();

            dtgBollaRefresh();
        }

        private void cmbListino_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbListino.SelectedIndex != -1)
                tbxPrezzoUnitario.Text = ((double)dbInterface.getListinoPrezzo(((KeyValuePair<string, int>)cmbListino.SelectedItem).Value)).ToString();
            else
                tbxPrezzoUnitario.Text = "0";
            cmbQuantita.SelectedIndex = 0;
        }

        private void btnAggiungi_Click(object sender, RoutedEventArgs e)
        {
            if (cmbListino.SelectedIndex < 0)
            {
                Dialog.MessageBox("Impossibile aggiornare bolla: è necessario selezionare un oggetto dal listino");
                return;
            }                

            if (dbInterface.insertBolla((int)cmbQuantita.SelectedItem, double.Parse(tbxPrezzoUnitario.Text), windowLavori.currentIDLavoro, ((KeyValuePair<string, int>)cmbListino.SelectedItem).Value))
                Dialog.MessageBox("Bolla aggiornata con successo!");
            else
                Dialog.MessageBox("Impossibile aggiornare bolla");

            refresh();
        }

        private void btnRimuovi_Click(object sender, RoutedEventArgs e)
        {
            if (dtgBolla.SelectedItems.Count == 0)
            {
                Dialog.MessageBox("Nessun elemento selezionato");
                return;
            }

            if (Dialog.MessageBox("Sei sicuro di voler rimuovere " + dtgBolla.SelectedItems.Count + " elemento/i?", "Conferma eliminazione", MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                return;

            foreach (DataRowView row in dtgBolla.SelectedItems)
                dbInterface.removeBolla(int.Parse(row["ID"].ToString()));

            dtgBollaRefresh();
        }
    }
}
