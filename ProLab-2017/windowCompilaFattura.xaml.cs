﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProLab_2017
{
    /// <summary>
    /// Interaction logic for windowCompilaFattura.xaml
    /// </summary>
    public partial class windowCompilaFattura : Window
    {
        struct LavoriEffettuatiEntry
        {
            public CheckBox chb;
            public int idLavoro;
            public string paziente;
        }

        struct LavoriInclusiEntry
        {
            public StackPanel stp;
            public int idLavoro;
            public double tot;
        }

        //stili
        Style styleStpInterno;


        private List<LavoriEffettuatiEntry> lavoriEffettuati;
        private List<LavoriInclusiEntry> lavoriInclusi;
        public windowCompilaFattura()
        {
            InitializeComponent();

            lavoriEffettuati = new List<LavoriEffettuatiEntry>();
            lavoriInclusi = new List<LavoriInclusiEntry>();

            //aggiungo handler per numeric tbx
            tbxMarca1.TextChanged += onMarcheDaBolloChanged;
            tbxMarca1.LostFocus += utils.numericTextBoxLostFocus;

            tbxMarca2.TextChanged += onMarcheDaBolloChanged;
            tbxMarca2.LostFocus += utils.numericTextBoxLostFocus;

            tbxMarca3.TextChanged += utils.numericTextBoxTextChanged;
            tbxMarca3.LostFocus += utils.numericTextBoxLostFocus;


            //stili
            styleStpInterno = new Style(typeof(StackPanel));
            styleStpInterno.Setters.Add(new Setter(BackgroundProperty, (Brush)(new BrushConverter()).ConvertFrom("#f48024")));
            styleStpInterno.Setters.Add(new Setter(MarginProperty, new Thickness(0, 0, 0, 10)));
        }

        private void onMarcheDaBolloChanged(object sender, TextChangedEventArgs e)
        {
            utils.numericTextBoxTextChanged(sender, e);
            updateLblTot();
        }

        private void cbChecked(object sender, RoutedEventArgs e)
        {
            CheckBox cb = (CheckBox) sender;
            LavoriEffettuatiEntry lavEff = lavoriEffettuati.Where(p => p.chb == cb).First();

            int idLavoro = lavEff.idLavoro;

            LavoriInclusiEntry l = new LavoriInclusiEntry();
            l.idLavoro = idLavoro;
            l.tot = 0;
            l.stp = new StackPanel();
            l.stp.Style = styleStpInterno;

            Label lab = new Label();
            lab.Foreground = Brushes.Black;
            lab.Background = Brushes.White;
            lab.FontSize = 24;
            lab.Margin = new Thickness(0, 0, 0, 10);
            lab.Padding = new Thickness(3);
            lab.Content = lavEff.paziente;

            l.stp.Children.Add(lab);

            SQLiteDataReader read = dbInterface.getBolla(idLavoro);
            while (read.Read())
            {
                string oggetto = Convert.ToString(read["Oggetto"]);
                int quantita = Convert.ToInt32(read["quantita"]);
                double prezzoUnitario = Convert.ToDouble(read["prezzoUnitario"]);

                string s = quantita + "x " + oggetto + " (Prezzo u. €" + prezzoUnitario + ")";

                lab = new Label();
                lab.FontSize = 18;
                lab.Content = s;

                l.tot += quantita * prezzoUnitario;
                l.stp.Children.Add(lab);
            }

            lavoriInclusi.Add(l);
            stpLavoriInclusi.Children.Add(l.stp);

            updateLblTot();

            btnCompilaFattura.IsEnabled = cmbModPagamento.IsEnabled = true;
        }

        private void cbUnchecked(object sender, RoutedEventArgs e)
        {
            CheckBox cb = (CheckBox) sender;
            int idLavoro = lavoriEffettuati.Where(p => p.chb == cb).FirstOrDefault().idLavoro;

            foreach (LavoriInclusiEntry l in lavoriInclusi)
            {
                if (l.idLavoro == idLavoro)
                {
                    stpLavoriInclusi.Children.Remove(l.stp);
                }
            }

            lavoriInclusi.RemoveAll(p => p.idLavoro == idLavoro);
            updateLblTot();

            if (lavoriInclusi.Count == 0)
                btnCompilaFattura.IsEnabled = cmbModPagamento.IsEnabled = false;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            MainWindow.wCompilaFattura = null;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            refresh();
        }

        private bool refreshing = false;
        private void refresh()
        {
            refreshing = true;

            dtpDataFattura.SelectedDate = DateTime.Today;

            btnCompilaFattura.IsEnabled = cmbModPagamento.IsEnabled = false;
            cmbModPagamento.SelectedIndex = 0;

            //combobox
            cmbCliente.Items.Clear();
            SQLiteDataReader read = dbInterface.getClienteDataGrid();
            while (read.Read())
            {
                int id = int.Parse(read["ID"].ToString());
                string name = read["NomeStudio"].ToString();

                cmbCliente.Items.Add(new KeyValuePair<string, int>(name, id));
            }

            updateLblTot();

            refreshing = false;
        }

        private void cmbCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lavoriEffettuati.Clear();
            stpLavoriEffettuati.Children.Clear();

            lavoriInclusi.Clear();
            stpLavoriInclusi.Children.Clear();

            if (!refreshing)
            {
                KeyValuePair<string, int> selected = (KeyValuePair<string, int>)cmbCliente.SelectedItem;
                int idCliente = selected.Value;
                SQLiteDataReader read = dbInterface.getListLavori(idCliente, -1, -1, true, false, false, true);

                while (read.Read())
                {
                    LavoriEffettuatiEntry l = new LavoriEffettuatiEntry();

                    l.idLavoro = Convert.ToInt32(read["ID"]);
                    l.paziente = Convert.ToString(read["Paziente"]);

                    l.chb = new CheckBox();
                    l.chb.RenderTransformOrigin = new Point(0, 0.5);
                    l.chb.RenderTransform = new ScaleTransform(1.3, 1.3);
                    l.chb.FontSize = 18;
                    l.chb.Margin = new Thickness(0, 0, 0, 10);
                    l.chb.Content = Convert.ToString(read["Paziente"]) + " - " + Convert.ToString(read["annotazioni"]);

                    l.chb.Checked += new RoutedEventHandler(cbChecked);
                    l.chb.Unchecked += new RoutedEventHandler(cbUnchecked);

                    lavoriEffettuati.Add(l);
                    stpLavoriEffettuati.Children.Add(l.chb);
                }
            }
        }

        private void btnCompilaFattura_Click(object sender, RoutedEventArgs e)
        {
            ComboBoxItem cmbItModPag = (ComboBoxItem)cmbModPagamento.SelectedItem;

            string data = utils.dpGetDateString(dtpDataFattura);
            string modPagamento = (string) cmbItModPag.Content;
            double b1 = double.Parse(tbxMarca1.Text);
            double b2 = double.Parse(tbxMarca2.Text);
            double b3 = double.Parse(tbxMarca3.Text);

            int[] lavori = new int[lavoriInclusi.Count];
            for (int i = 0; i < lavoriInclusi.Count; i++)
            {
                lavori[i] = lavoriInclusi[i].idLavoro;
            }

            if (dbInterface.insertFattura(
                    data,
                    modPagamento,
                    b1,
                    b2,
                    b3,
                    lavori
                ))
            {
                Dialog.MessageBox("Fattura inserita con successo");

                refresh();
            }
            else
            {
                Dialog.MessageBox("Impossibile inserire fattura");
            }
        }

        private void updateLblTot()
        {
            double tot = 0;

            foreach (LavoriInclusiEntry l in lavoriInclusi)
            {
                tot += l.tot;
            }

            try { tot += double.Parse(tbxMarca1.Text); } catch { }
            try { tot += double.Parse(tbxMarca2.Text); } catch { }
            try { tot += double.Parse(tbxMarca3.Text); } catch { }

            lblTotale.Content = "Totale: € " + tot.ToString();
        }
    }
}
