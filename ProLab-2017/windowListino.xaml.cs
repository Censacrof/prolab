﻿using System;
using System.Data;
using System.Data.SQLite;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace ProLab_2017
{
    /// <summary>
    /// Logica di interazione per windowListino.xaml
    /// </summary>
    public partial class windowListino : Window
    {
        public windowListino()
        {
            InitializeComponent();


            //aggiungo handler per dare comportamento da numeric text box
            tbxPrezzo.TextChanged += utils.numericTextBoxTextChanged;
            tbxPrezzo.LostFocus += utils.numericTextBoxLostFocus;
        }
        
        private void Window_Closed(object sender, EventArgs e)
        {
            MainWindow.wListino = null;
        }

        DataTable dataTableListino;
        private void dtgListinoRefresh()
        {
            dataTableListino = new DataTable();
            SQLiteDataReader dr = dbInterface.getListino();
            dataTableListino.Load(dr);
            dtgListino.DataContext = dataTableListino.DefaultView;
        }

        private void btnInserisci_Click(object sender, RoutedEventArgs e)
        {
            double prezzo;

            try { prezzo = Math.Truncate(double.Parse(tbxPrezzo.Text) * 100); } catch { prezzo = 0; }
            if (dbInterface.insertListino(tbxDescrizione.Text, prezzo))
                Dialog.MessageBox("Listino aggiornato con successo!");
            else
                Dialog.MessageBox("Impossibile aggiornare listino");

            dtgListinoRefresh();
            tbxDescrizione.Text = "";
            tbxPrezzo.Text = "";

            tbxDescrizione.Focus();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            dtgListinoRefresh();
        }

        private void btnRimuovi_Click(object sender, RoutedEventArgs e)
        {
            if (dtgListino.SelectedItems.Count == 0)
            {
                Dialog.MessageBox("Nessun elemento selezionato");
                return;
            }

            if (Dialog.MessageBox("Sei sicuro di voler rimuovere " + dtgListino.SelectedItems.Count + " elemento/i?", "Conferma eliminazione", MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                return;

            foreach (DataRowView row in dtgListino.SelectedItems)
                dbInterface.removeListino(int.Parse(row["ID"].ToString()));                

            dtgListinoRefresh();
        }
    }
}
