﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using mshtml;

namespace ProLab_2017
{
    /// <summary>
    /// Interaction logic for windowPlanning.xaml
    /// </summary>
    public partial class windowPlanning : Window
    {
        public windowPlanning()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            webPlanning.NavigateToString(File.ReadAllText("planning.html"));
            webPlanning.LoadCompleted += new System.Windows.Navigation.LoadCompletedEventHandler(webPlanningPageLoaded);
        }

        private void webPlanningPageLoaded(object sender, EventArgs e)
        {
            refreshPlanning();
        }

        private void refreshPlanning()
        {
            SQLiteDataReader dr = dbInterface.getPlanningDataProva();
            List<List<string> > gg = new List<List<string>>(new List<string>[7]);

            for (int i = 0; i < 7; i++)
            { 
                List<string> hh = new List<string>(new string[24]);
                gg[i] = hh;
            }


            while (dr.Read())
            {
                int g = Convert.ToInt32(dr["giornoSettimana"]);

                string fullh = Convert.ToString(dr["ora"]);
                int colons = fullh.IndexOf(':');

                int h;
                int m;

                try
                {
                    h = int.Parse(fullh.Substring(0, colons));
                    m = int.Parse(fullh.Substring(colons + 1));
                } catch (Exception e)
                {
                    utils.DebugMessage(e.ToString());
                    continue;
                }

                if (h < 8 || h > 20 || m < 0 || m > 59) continue;
                int cellIndex = (h - 8) * 2;
                if (m >= 30 && h < 20) cellIndex++;

                gg[g][cellIndex] += Convert.ToString(dr["nomeStudio"]) + "<br />" + Convert.ToString(dr["paziente"]);

                string notes = Convert.ToString(dr["annotazioni"]);
                if (notes != "" && notes != null)
                    gg[g][cellIndex] += "<br />" + notes;
            }

            string planstr = JsonConvert.SerializeObject(gg);
            webPlanning.InvokeScript("refreshPlanning", planstr);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            MainWindow.wPlanning = null;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            mshtml.IHTMLDocument2 doc = webPlanning.Document as mshtml.IHTMLDocument2;
            doc.execCommand("Print", true, null);
        }
    }
}
