﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProLab_2017.dialog
{
    /// <summary>
    /// Interaction logic for InputBox.xaml
    /// </summary>
    public partial class dialogInputBox : Window
    {
        public dialogInputBox()
        {
            InitializeComponent();
        }

        private bool okPressed = false;
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (okPressed)
                Dialog.returnString = txbValue.Text;
            else Dialog.returnString = null;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            okPressed = true;
            this.Close();
        }

        private void txbValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                btnOk_Click(null, null);
            }
        }
    }
}
