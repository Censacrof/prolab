﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProLab_2017.dialog
{
    /// <summary>
    /// Interaction logic for dialogMessageBox.xaml
    /// </summary>
    public partial class dialogMessageBox : Window
    {
        public dialogMessageBox()
        {
            InitializeComponent();
        }

        private void btnA_Click(object sender, RoutedEventArgs e)
        {
            Dialog.returnInt = 0;
            this.Close();
        }

        private void btnB_Click(object sender, RoutedEventArgs e)
        {
            Dialog.returnInt = 1;
            this.Close();
        }

        private void btnC_Click(object sender, RoutedEventArgs e)
        {
            Dialog.returnInt = 2;
            this.Close();
        }
    }
}
