﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ProLab_2017
{
    public static class Dialog
    {
        public static string returnString;
        public static int returnInt;

        public static string InputBox(string message = "", string title = "")
        {
            dialog.dialogInputBox d = new dialog.dialogInputBox();
            d.txbMessage.Text = message;
            d.txbValue.Focus();
            d.Title = title;
            d.ShowDialog();

            return returnString;
        }

        public static string InputPasswordBox(string message = "", string title = "")
        {
            dialog.dialogInputPasswordBox d = new dialog.dialogInputPasswordBox();
            d.txbMessage.Text = message;
            d.txbValue.Focus();
            d.Title = title;
            d.ShowDialog();

            return returnString;
        }

        public static MessageBoxResult MessageBox(string message = "", string title = "", MessageBoxButton btn = MessageBoxButton.OK)
        {
            dialog.dialogMessageBox d = new dialog.dialogMessageBox();
            d.txbMessage.Text = message;
            d.Title = title;

            switch (btn)
            {
                case MessageBoxButton.OK:
                    d.btnA.Content = "Ok";
                    d.btnA.Focus();

                    d.btnB.IsEnabled = false;
                    d.btnB.Visibility = Visibility.Collapsed;

                    d.btnC.IsEnabled = false;
                    d.btnC.Visibility = Visibility.Collapsed;

                    returnInt = -1;
                    d.ShowDialog();

                    if (returnInt == 0) return MessageBoxResult.OK;
                    else return MessageBoxResult.None;

                case MessageBoxButton.OKCancel:
                    d.btnA.Content = "Ok";
                    d.btnB.Content = "Annulla";
                    d.btnB.Focus();

                    d.btnC.IsEnabled = false;
                    d.btnC.Visibility = Visibility.Collapsed;

                    returnInt = -1;
                    d.ShowDialog();

                    if (returnInt == 0) return MessageBoxResult.OK;
                    else if (returnInt == 1) return MessageBoxResult.Cancel;
                    else return MessageBoxResult.None;

                case MessageBoxButton.YesNo:
                    d.btnA.Content = "Si";
                    d.btnB.Content = "No";
                    d.btnA.Focus();

                    d.btnC.IsEnabled = false;
                    d.btnC.Visibility = Visibility.Collapsed;

                    returnInt = -1;
                    d.ShowDialog();

                    if (returnInt == 0) return MessageBoxResult.Yes;
                    else if (returnInt == 1) return MessageBoxResult.No;
                    else return MessageBoxResult.None;

                case MessageBoxButton.YesNoCancel:
                    d.btnA.Content = "Si";
                    d.btnB.Content = "No";
                    d.btnC.Content = "Annulla";
                    d.btnC.Focus();

                    returnInt = -1;
                    d.ShowDialog();

                    if (returnInt == 0) return MessageBoxResult.Yes;
                    else if (returnInt == 1) return MessageBoxResult.No;
                    else if (returnInt == 2) return MessageBoxResult.Cancel;
                    else return MessageBoxResult.None;
            }

            return MessageBoxResult.None;
        }
    }
}
