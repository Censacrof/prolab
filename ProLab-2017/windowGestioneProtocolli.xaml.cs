﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace ProLab_2017
{
    /// <summary>
    /// Interaction logic for windowGestioneProtocolli.xaml
    /// </summary>
    public partial class windowGestioneProtocolli : Window
    {
        private bool unsavedModifications = false;

        public windowGestioneProtocolli()
        {
            InitializeComponent();

            utils.populateCmbDispositivo(cmbDispositivo);
            utils.populateCmbOperatore(cmbOperatore);

            cmbFascicolo.DisplayMemberPath = "Value";
            cmbFascicolo.SelectedValuePath = "Key";
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            MainWindow.wGestioneProtocolli = null;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            refresh();
        }

        
        private void refresh()
        {
            //triggero l'evento "Selection Changed" di cmbDispositivo
            int lastSelected = cmbDispositivo.SelectedIndex;
            cmbDispositivo.SelectedIndex = -1;
            cmbDispositivo.SelectedIndex = lastSelected != -1 ? lastSelected : 0;

            //pulisco i controlli
            txbDescrizione.Text = "";
            cmbOperatore.SelectedIndex = 0;

        }

        private void cmbDispositivo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbDispositivo.SelectedIndex == -1)
                return;

            KeyValuePair<int, string>  dispSel = (KeyValuePair < int, string >) cmbDispositivo.SelectedItem;
            SQLiteDataReader read = dbInterface.getFascicolo(dispSel.Value);

            //setto cmbFascicolo
            cmbFascicolo.Items.Clear();            

            while (read.Read())
            {
                cmbFascicolo.Items.Add(new KeyValuePair<int, string>(Convert.ToInt32(read["ID"]), Convert.ToString(read["nome"])));
            }

            cmbFascicolo.SelectedIndex = 0;
        }

        private void btnNuovoFascicolo_Click(object sender, RoutedEventArgs e)
        {
            if (cmbDispositivo.SelectedIndex == -1)
            {
                Dialog.MessageBox("Seleziona prima un tipo di dispositivo");
                return;
            }

            string nomeFascicolo = Dialog.InputBox("Inserisci il nome del nuovo fascicolo", "Nuovo Fascicolo");
            KeyValuePair<int, string> dispSel = (KeyValuePair<int, string>)cmbDispositivo.SelectedItem;

            if (String.IsNullOrEmpty(nomeFascicolo))
            {
                Dialog.MessageBox("Inserisci un nome valido");
                return;
            }

            if (Dialog.MessageBox("Sei sicuro di voler inserire il fascicolo: '" + nomeFascicolo + "'\ndi tipo: '" + dispSel.Value + "' ?", "Conferma inserimento fascicolo", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                if (dbInterface.insertFascicolo(nomeFascicolo, dispSel.Value))
                {
                    Dialog.MessageBox("Fascicolo inserito con successo");
                    refresh();
                }                    
                else
                    Dialog.MessageBox("Impossibile inserire fascicolo");
            }
        }

        private void btnEliminaFascicolo_Click(object sender, RoutedEventArgs e)
        {
            if (cmbFascicolo.SelectedIndex == -1)
            {
                Dialog.MessageBox("Seleziona prima un fascicolo");
                return;
            }

            KeyValuePair<int, string> fascSel = (KeyValuePair<int, string>)cmbFascicolo.SelectedItem;
            if (Dialog.MessageBox("Sei sicuro di voler eliminare il fascicolo '" + fascSel.Value + "' ?", "Conferma eliminazione fascicolo", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                dbInterface.removeFascicolo(fascSel.Key);
                refresh();
            }
        }

        private void addFase(string descrizione, string operatore)
        {
            TextChangedEventHandler handlerA = null;
            SelectionChangedEventHandler handlerB = null;

            TextBox txbA = new TextBox();
            ComboBox cmbB = new ComboBox();

            txbA.Text = descrizione;
            txbA.Width = 450;
            txbA.Margin = new Thickness(3, 0, 45, 0);
            Panel.SetZIndex(txbA, 3);
            
            utils.populateCmbOperatore(cmbB);
            int i = 0; bool found = false;
            foreach (KeyValuePair<int, string> item in cmbB.Items)
            {
                if (item.Value == operatore)
                {
                    cmbB.SelectedIndex = i;
                    found = true;

                    break;
                }                   

                i++;
            }
            if (!found) cmbB.SelectedIndex = 0;
            cmbOperatore.Width = 180;
            Panel.SetZIndex(cmbB, 3);

            //stack panel h
            StackPanel sp = new StackPanel();
            sp.Orientation = Orientation.Horizontal;
            sp.HorizontalAlignment = HorizontalAlignment.Stretch;

            sp.Children.Add(txbA);
            sp.Children.Add(cmbB);

            lsbFasi.Items.Add(sp);

            txbA.TextChanged += handlerA = new TextChangedEventHandler(delegate (object s, TextChangedEventArgs e) { unsavedModifications = true; });
            cmbB.SelectionChanged += handlerB = new SelectionChangedEventHandler(delegate (object s, SelectionChangedEventArgs e) { unsavedModifications = true; });
        }

        private void btnInserisciFase_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(txbDescrizione.Text))
            {
                Dialog.MessageBox("Inserire prima una descrizione e selezionare un operatore");
                return;
            }

            KeyValuePair<int, string> opSel = (KeyValuePair < int, string >)cmbOperatore.SelectedItem;

            addFase(txbDescrizione.Text, opSel.Value);

            txbDescrizione.Text = "";

            unsavedModifications = true;
        }

        private void btnMuoviSu_Click(object sender, RoutedEventArgs e)
        {
            if (lsbFasi.SelectedIndex <= 0 || lsbFasi.Items.Count <= 1)
                return;

            int targetIndex = lsbFasi.SelectedIndex - 1;
            StackPanel swap = (StackPanel) lsbFasi.SelectedItem;

            lsbFasi.Items.RemoveAt(lsbFasi.SelectedIndex);
            lsbFasi.Items.Insert(targetIndex, swap);

            lsbFasi.SelectedIndex = targetIndex;

            unsavedModifications = true;
        }

        private void btnMuoviGiu_Click(object sender, RoutedEventArgs e)
        {
            if (lsbFasi.SelectedIndex == -1 || lsbFasi.Items.Count <= 1 || lsbFasi.SelectedIndex == lsbFasi.Items.Count - 1)
                return;

            int targetIndex = lsbFasi.SelectedIndex + 1;
            StackPanel swap = (StackPanel)lsbFasi.SelectedItem;

            lsbFasi.Items.RemoveAt(lsbFasi.SelectedIndex);
            lsbFasi.Items.Insert(targetIndex, swap);

            lsbFasi.SelectedIndex = targetIndex;

            unsavedModifications = true;
        }

        private void btnRimuoviFase_Click(object sender, RoutedEventArgs e)
        {
            if (lsbFasi.SelectedIndex == -1)
            {
                Dialog.MessageBox("Selezionare prima una fase");
                return;
            }

            lsbFasi.Items.RemoveAt(lsbFasi.SelectedIndex);
        }

        private void btnSalvaFasi_Click(object sender, RoutedEventArgs e)
        {
            KeyValuePair<int, string> selFasc = (KeyValuePair<int, string>) cmbFascicolo.SelectedItem;

            if (dbInterface.insertDescrizioneFascicolo(selFasc.Key, lsbFasi))
            {
                Dialog.MessageBox("Fasi lavorative aggioranate con successo");
                unsavedModifications = false;
            } else Dialog.MessageBox("Impossibile aggiornare le fasi lavorative");
        }

        private void cmbFascicolo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lsbFasi.Items.Clear();

            if (cmbFascicolo.SelectedIndex == -1)
                return;

            KeyValuePair<int, string> selFasc = (KeyValuePair<int, string>) cmbFascicolo.SelectedItem;

            SQLiteDataReader read = dbInterface.getDescFascicolo(selFasc.Key);

            while (read.Read())
            {
                addFase(Convert.ToString(read["descrizione"]), Convert.ToString(read["operatore"]));
            }

            unsavedModifications = false;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (unsavedModifications)
            {
                switch (Dialog.MessageBox("Vuoi salvare le modifiche apportate al fascicolo selezionato?", "Attenzione", MessageBoxButton.YesNoCancel))
                {
                    case MessageBoxResult.Yes:
                        btnSalvaFasi_Click(null, new RoutedEventArgs());
                        break;

                    case MessageBoxResult.No:

                        break;

                    default:
                        e.Cancel = true;
                        break;
                }
            }
        }
        
        private void btnAvvertenze_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
