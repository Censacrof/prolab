﻿using System.Linq;
using System.IO;
using System.Data.SQLite;
using System;
using System.Windows;
using System.Collections.Generic;
using System.Windows.Controls;

namespace ProLab_2017
{
    static class dbInterface
    {
        const string TARGETDBVERSION = "1.0";

        public static string SQLITEFILE = dataInterface.dataFolderPath + @"\db.plb";
        const string DBGENERATE = "dbGenerate.sql";
        public static SQLiteConnection _dbConnect;
        static SQLiteCommand _dbCommand;

        public static bool init()
        {
            bool isNew = false;
            if (!File.Exists(SQLITEFILE))
            {
                SQLiteConnection.CreateFile(SQLITEFILE);
                isNew = true;
            }

            _dbConnect = new SQLiteConnection("Data Source=" + SQLITEFILE + ";Version=3;");
            _dbConnect.Open();

            if (isNew)
            {
                try
                {
                    _dbCommand = new SQLiteCommand(File.ReadAllText(DBGENERATE), _dbConnect);
                    _dbCommand.ExecuteNonQuery();
                } catch (Exception e)
                {
                    utils.DebugMessage(e.ToString());
                    
                    if (File.Exists(SQLITEFILE))
                    {
                        _dbConnect.Close();
                        File.Delete(SQLITEFILE);
                    }

                    Dialog.MessageBox("Impossibile inizializzare la banca dati", "Errore fatale");
                    
                    return false;
                }                    
            }

            //abilito controllo di chiave esterna
            _dbCommand = new SQLiteCommand("PRAGMA foreign_keys = ON;", _dbConnect);
            _dbCommand.ExecuteNonQuery();

            if (dbVarGet("dbVersion") != TARGETDBVERSION)
            {
                Dialog.MessageBox("Il database selezionato non è compatibile con questa versione del programma", "Errore fatale");
                return false; //TODO: in questo caso dare la possibilità di selezionare un altro database
            }            

            return true;
        }
        

        public static Nullable<long> getLastInsertID()
        {
            _dbCommand = new SQLiteCommand(@"select last_insert_rowid()", _dbConnect);
            object test = _dbCommand.ExecuteScalar();

            return (Nullable<long>) _dbCommand.ExecuteScalar();
        }


        //USARE PER QUERY DI INSERIMENTO
        public static bool insQuery(string query, object[] param)
        {
            try
            {
                _dbCommand = new SQLiteCommand(query, _dbConnect);

                int i = 1;
                foreach (object p in param)
                {
                    _dbCommand.Parameters.Add(new SQLiteParameter("param" + i, p));

                    i++;
                }

                return _dbCommand.ExecuteNonQuery() > 0 ? true : false;
            } catch (Exception e) { utils.DebugMessage(e.ToString()); return false; }
        }        

        public static bool notCatchingInsQuery(string query, object[] param)
        {
            _dbCommand = new SQLiteCommand(query, _dbConnect);

            int i = 1;
            foreach (object p in param)
            {
                _dbCommand.Parameters.Add(new SQLiteParameter("param" + i, p));

                i++;
            }

            return _dbCommand.ExecuteNonQuery() > 0 ? true : false;
        }

        /*------------ DBVAR ------------*/
        public static string dbVarGet(string var)
        {
            try
            {
                _dbCommand = new SQLiteCommand("select value from dbVar where var = '" + var + "' limit 1;", _dbConnect);
                object res = _dbCommand.ExecuteScalar();
                return res != null ? Convert.ToString(res) : null;
            }
            catch (Exception e) { utils.DebugMessage(e.ToString()); return null; }
        }


        public static bool dbVarSet(string var, string val)
        {
            return insQuery("insert into dbVar(var, value) values (?, ?);", new object[] { var, val });
        }

        /*------------ LISTINO ------------*/
        public static bool insertListino(string descrizione, double prezzo)
        {
            if (descrizione == "") return false;

            return insQuery("insert into listino(descrizione, prezzo) values (?, ?);", new object[] { descrizione, prezzo });
        }

        public static SQLiteDataReader getListino()
        {
            try
            {
                _dbCommand = new SQLiteCommand("select ID, descrizione as Descrizione, prezzo / 100 as Prezzo from listino;", _dbConnect);
                return _dbCommand.ExecuteReader();
            } catch (Exception e) { utils.DebugMessage(e.ToString()); return null; }
           
        }

        public static object getListinoPrezzo(int ID)
        {
            try
            {
                _dbCommand = new SQLiteCommand("select prezzo / 100 as Prezzo from listino where ID = " + ID + ";", _dbConnect);
                return _dbCommand.ExecuteScalar();
            } catch (Exception e) { utils.DebugMessage(e.ToString()); return null; }
        }

        public static void removeListino(int ID)
        {
            try
            {
                _dbCommand = new SQLiteCommand("delete from listino where ID = " + ID + ";", _dbConnect);
                _dbCommand.ExecuteNonQuery();
            } catch (Exception e) { utils.DebugMessage(e.ToString()); return; }

        }


        /*------------ CLIENTE ------------*/

        public static bool insertCliente(string nomeStudio, string indirizzo, string partitaIva, string codFiscale, string telefono, string iban, string bancaAppoggio, string email)
        {
            //_dbCommand = new SQLiteCommand("insert into cliente(nomeStudio, indirizzo, partitaIva, codFiscale, telefono, iban, bancaAppoggio, email) values ('" + nomeStudio + "', '" + indirizzo + "', '" + partitaIva + "', '" + codFiscale + "', '" + telefono + "', '" + iban + "', '" + bancaAppoggio + "', '" + email + "');", _dbConnect);
            return insQuery("insert into cliente(nomeStudio, indirizzo, partitaIva, codFiscale, telefono, iban, bancaAppoggio, email) values(?, ?, ?, ?, ?, ?, ?, ?);",
                new object[] {
                    nomeStudio,
                    indirizzo,
                    partitaIva,
                    codFiscale,
                    telefono,
                    iban,
                    bancaAppoggio,
                    email
                });
        }

        public static SQLiteDataReader getClienteDataGrid()
        {
            try
            {
                _dbCommand = new SQLiteCommand("select ID, nomeStudio as NomeStudio, partitaIva as PartitaIva, codFiscale as CodiceFiscale, telefono as Telefono, iban as Iban, bancaAppoggio as BancaDiAppoggio, email as Email from cliente;", _dbConnect);
                return _dbCommand.ExecuteReader();
            } catch (Exception e) { utils.DebugMessage(e.ToString()); return null; }

        }

        public static void removeCliente(int ID)
        {
            try
            {
                _dbCommand = new SQLiteCommand("delete from cliente where ID = " + ID + ";", _dbConnect);
                _dbCommand.ExecuteNonQuery();
            } catch (Exception e) { utils.DebugMessage(e.ToString()); return; }
        }


        /*------------ LAVORO ------------*/

        public static bool insertLavoro(string dataLavoro, string dataPrescrizione, string nomePaziente, string cognomePaziente, int idCliente, string annotazioni)
        {
            return insQuery("insert into lavoro(dataLavoro, dataPrescrizione, nomePaziente, cognomePaziente, idCliente, annotazioni) values(?, ?, ?, ?, ?, ?);",
                new object[] {
                    dataLavoro,
                    dataPrescrizione,
                    nomePaziente,
                    cognomePaziente,
                    idCliente,
                    annotazioni
                });
        }


        public static bool updateLavoro(int ID, string dataLavoro, string dataPrescrizione, string nomePaziente, string cognomePaziente, int idCliente, string annotazioni, int chiuso, int consegnato)
        {
            if (chiuso < 0 || chiuso > 1) chiuso = 0;
            if (consegnato < 0 || consegnato > 1) consegnato = 0;

            return insQuery("update lavoro set dataLavoro = ?, dataPrescrizione = ?, nomePaziente = ?, cognomePaziente = ?, idCliente = ?, annotazioni = ?, chiuso = ?, consegnato = ? where ID = '" + ID + "';",
                    new object[] {
                        dataLavoro,
                        dataPrescrizione,
                        nomePaziente,
                        cognomePaziente,
                        idCliente,
                        annotazioni,
                        chiuso,
                        consegnato
                    });
        }

        public static bool updateLavoroScadenze(int ID, string dataRitiro, string dataConsegna)
        {
            return insQuery("update lavoro set dataRitiro = ?, dataConsegna = ? where ID = " + ID + ";",
                new object[] {
                    dataRitiro,
                    dataConsegna
                });
        }

        public static SQLiteDataReader getLavoro(int ID)
        {
            try
            {
                _dbCommand = new SQLiteCommand("select ID, dataLavoro, dataPrescrizione, dataRitiro, date(dataConsegna) as dataConsegna, strftime('%H:%M', dataConsegna) as oraConsegna, cognomePaziente, nomePaziente, annotazioni, ultimoAccesso, idCliente, chiuso, consegnato from lavoro where ID = " + ID + ";", _dbConnect);
                return _dbCommand.ExecuteReader();
            } catch (Exception e) { utils.DebugMessage(e.ToString()); return null; }
        }

        public static Nullable<int> getLastIDLavoro()
        {
            _dbCommand = new SQLiteCommand(@"select max(ID) from lavoro;", _dbConnect);

            Nullable<int> res = (int)_dbCommand.ExecuteScalar();
            return res;
        }

        /*------------ LISTLAVORI ------------*/
        public static SQLiteDataReader getListLavori(int idCliente = -1, int mese = -1, int anno = -1, bool chiuso = false, bool consegnato = false, bool limit = false, bool nonFatturato = false)
        {
            string query = "select distinct l.ID, l.dataLavoro, c.nomeStudio as Cliente, l.dataPrescrizione, l.cognomePaziente || \" \" || l.nomePaziente as Paziente, l.annotazioni, l.dataRitiro, l.dataConsegna from lavoro l join cliente c on c.ID = l.idCliente" + (nonFatturato ? " join bolla b on b.idLavoro = l.ID" : "");

            query += " where idCliente = " + (idCliente != -1 ? idCliente.ToString() : "idCliente");

            if (mese != -1)
                query += " and cast(strftime('%m', dataLavoro) as integer) = " + mese;
            if (anno != -1)
                query += " and cast(strftime('%Y', dataLavoro) as integer) = " + anno;

            if (chiuso)
                query += " and l.chiuso = 1";
            if (consegnato)
                query += " and l.consegnato = 1";

            if (nonFatturato)
                query += " and b.idFattura is null";
            
            query += " order by datetime(l.ultimoAccesso) desc";

            if (limit)
                query += " limit 10";
            query += ";";

            try
            {
                _dbCommand = new SQLiteCommand(query, _dbConnect);
                return _dbCommand.ExecuteReader();
            } catch (Exception e) { utils.DebugMessage(e.ToString()); return null; }
        }

        public static void removeLavoro(int ID)
        {
            try
            {
                _dbCommand = new SQLiteCommand("delete from lavoro where ID = " + ID + ";", _dbConnect);
                _dbCommand.ExecuteNonQuery();
            } catch (Exception e) { utils.DebugMessage(e.ToString()); return; }
        }

        /*------------ DATAPROVA ------------*/

        public static bool insertProva(int idLavoro, string data, string ora, string annotazioni)
        {
            return insQuery("insert into dataProva(idLavoro, data, ora, annotazioni) values(?, ?, ?, ?);",
                new object[] {
                    idLavoro,
                    data,
                    ora,
                    annotazioni
                });
        }
    
        public static SQLiteDataReader getProve(int idLavoro)
        {
            try
            {
                _dbCommand = new SQLiteCommand("select ID, strftime('%d/%m/%Y', data) as Data, ora as Ora, annotazioni as Annotazioni from dataProva where idLavoro = " + idLavoro + ";", _dbConnect);
                return _dbCommand.ExecuteReader();
            } catch (Exception e) { utils.DebugMessage(e.ToString()); return null; }
        }

        public static void removeProva(int ID)
        {
            try
            {
                _dbCommand = new SQLiteCommand("delete from dataProva where ID = " + ID + ";", _dbConnect);
                _dbCommand.ExecuteNonQuery();
            } catch (Exception e) { utils.DebugMessage(e.ToString()); return; }
        }

        internal static SQLiteDataReader getScadenzeLavori()
        {
            try
            {
                _dbCommand = new SQLiteCommand("select v.idLavoro, c.nomeStudio as Cliente,  l.cognomePaziente || ' ' || l.nomePaziente as Paziente, v.Ora, v.Annotazioni from vDataProvaOggi v join lavoro l on v.idLavoro = l.ID join cliente c on l.idCliente = c.ID where date(Data) = date() order by time(Ora);", _dbConnect);
                return _dbCommand.ExecuteReader();
            }
            catch (Exception e) { utils.DebugMessage(e.ToString()); return null; }
        }

        /*------------ BOLLA ------------*/
        public static SQLiteDataReader getBolla(Nullable<int> idLavoro)
        {
            if (idLavoro == null) idLavoro = -1;

            try
            {
                _dbCommand = new SQLiteCommand("select b.ID, l.descrizione as Oggetto, b.quantita, b.prezzoUnitario from bolla b join listino l on b.idListino = l.ID where b.idLavoro = " + idLavoro + ";", _dbConnect);
                return _dbCommand.ExecuteReader();
            } catch (Exception e) { utils.DebugMessage(e.ToString()); return null; }
        }


        public static bool insertBolla(int quantita, double prezzounitario, Nullable<int> idLavoro, int idListino)
        {
            if (idLavoro == null) idLavoro = -1;

            return insQuery("insert into bolla(quantita, prezzounitario, idLavoro, idListino) values(?, ?, ?, ?);",
                new object[] {
                    quantita,
                    prezzounitario,
                    idLavoro,
                    idListino
                });
        }


        public static void removeBolla(int ID)
        {
            try
            {
                _dbCommand = new SQLiteCommand("delete from bolla where ID = " + ID + ";", _dbConnect);
                _dbCommand.ExecuteNonQuery();
            } catch (Exception e) { utils.DebugMessage(e.ToString()); return; }
        }

        public static int getCountBolla(int idLavoro)
        {
            _dbCommand = new SQLiteCommand("select count(*) from bolla where idLavoro = " + idLavoro + ";", _dbConnect);

            return Convert.ToInt32(_dbCommand.ExecuteScalar());
        }


        /*------------ PLANNING  ------------*/
        public static SQLiteDataReader getPlanningDataProva()
        {
            try
            {
                _dbCommand = new SQLiteCommand("select * from vDataProvaSettimana;", _dbConnect);
                return _dbCommand.ExecuteReader();
            }
            catch (Exception e) { utils.DebugMessage(e.ToString()); return null; }
        }


        /*------------ FATTURA  ------------*/
        public static bool insertFattura(string data, string modPagamento, double marcaBollo1, double marcaBollo2, double marcaBollo3, int[] lavori)
        {
            if (lavori.Length == 0)
                return false;

            using (SQLiteTransaction tr = _dbConnect.BeginTransaction())
            {
                if (!insQuery("insert into fattura(data, modPagamento, marcaBollo1, marcaBollo2, marcaBollo3) values(?, ?, ?, ?, ?);",
                    new object[] {
                       data,
                       modPagamento,
                       marcaBollo1,
                       marcaBollo2,
                       marcaBollo3
                    }))
                {
                    tr.Rollback();
                    return false;
                }

                _dbCommand.CommandText = "select last_insert_rowid()";
                long idFattura = (long) _dbCommand.ExecuteScalar();

                foreach (int idLavoro in lavori)
                {
                    if (!insQuery("update bolla set idFattura = ? where idLavoro = ?;",
                        new object[] {
                            idFattura,
                            idLavoro
                        }))
                    {
                        tr.Rollback();
                        return false;
                    }
                }

                tr.Commit();
            }

            return true;
        }

        public static SQLiteDataReader getFattura(int idCliente = -1, int anno = -1)
        {
            try
            {
                string query = "select f.ID as \"# Fattura\", c.nomeStudio as Cliente, replace(group_concat(distinct l.cognomePaziente || \" \" || l.nomePaziente), ',', ', ') as Pazienti, strftime('%d/%m/%Y', f.data) as Data, (sum(b.prezzoUnitario * b.quantita) + f.marcaBollo1 + f.marcaBollo2 + f.marcaBollo3) || \" €\" as Totale from fattura f join bolla b on b.idFattura = f.ID join lavoro l on b.idLavoro = l.ID join cliente c on l.idCliente = c.ID ";

                if (idCliente >= 0 || anno >= 0)
                    query += "where ";

                if (idCliente >= 0)
                    query += "c.ID = " + idCliente + " ";

                if (anno >= 0)
                {
                    if (idCliente >= 0)
                        query += "and ";

                    query += "strftime('%Y', f.data) = '" + anno + "' ";
                }

                query += "group by f.ID order by datetime(f.data) desc;";

                _dbCommand = new SQLiteCommand(query, _dbConnect);
                return _dbCommand.ExecuteReader();
            }
            catch (Exception e) { utils.DebugMessage(e.ToString()); return null; }
        }


        public static void fatturaFillMissingFatturaDati(ref DatiFattura dati, int idFattura)
        {
            string query = "select f.ID, f.modPagamento, cast(f.marcaBollo1 as text) as marcaBollo1, cast(f.marcaBollo2 as text) as marcaBollo2, cast(f.marcaBollo3 as text) as marcaBollo3, c.nomeStudio as cliente, c.indirizzo as indirizzoCliente from fattura f join bolla b on b.idFattura = f.ID join lavoro l on b.idLavoro = l.ID join cliente c on l.idCliente = c.ID where f.ID = " + idFattura + " group by f.ID limit 1;";
            
            SQLiteDataReader read;
            try
            {
                _dbCommand = new SQLiteCommand(query, _dbConnect);
                read = _dbCommand.ExecuteReader();
            }
            catch (Exception e) { utils.DebugMessage(e.ToString());  return; }

            while (read.Read()) 
            {
                dati.cNomeStudio = Convert.ToString(read["cliente"]);
                dati.cIndirizzo = Convert.ToString(read["indirizzoCliente"]);

                dati.marcaBollo1 = Convert.ToDouble(read["marcaBollo1"]);
                dati.marcaBollo2 = Convert.ToDouble(read["marcaBollo2"]);
                dati.marcaBollo3 = Convert.ToDouble(read["marcaBollo3"]);
                dati.modPag = Convert.ToString(read["modPagamento"]);
            }

            query = "select b.idFattura, b.idLavoro, b.quantita, b.prezzoUnitario, l.descrizione from bolla b join listino l on b.idListino = l.ID where idFattura = " + idFattura + " order by b.idLavoro;";
            try
            {
                _dbCommand = new SQLiteCommand(query, _dbConnect);
                read = _dbCommand.ExecuteReader();
            }
            catch (Exception e) { utils.DebugMessage(e.ToString()); return; }

            dati.totNetto = 0;
            dati.items = new List<DatiFatturaItem>();
            while (read.Read())
            {
                DatiFatturaItem item = new DatiFatturaItem();

                item.descrizione = Convert.ToString(read["descrizione"]);
                item.quantita = Convert.ToInt32(read["quantita"]);
                item.prezzoUni = Convert.ToDouble(read["prezzoUnitario"]);
                item.importo = item.quantita * item.prezzoUni;

                dati.totNetto += item.importo;

                dati.items.Add(item);
            }
        }


        /*------------ FASCICOLO ------------*/

        public static SQLiteDataReader getFascicolo(string dispositivo)
        {
            try
            {
                string query = "select * from fascicolo where dispositivo = '" + dispositivo + "'";

                _dbCommand = new SQLiteCommand(query, _dbConnect);
                return _dbCommand.ExecuteReader();
            }
            catch (Exception e) { utils.DebugMessage(e.ToString()); return null; }
        }

        public static bool insertFascicolo(string nome, string dispositivo)
        {
            return insQuery("insert into fascicolo(nome, dispositivo) values(?, ?);",
                new object[] {
                    nome,
                    dispositivo
                });
        }

        public static void removeFascicolo(int ID)
        {
            try
            {
                _dbCommand = new SQLiteCommand("delete from fascicolo where ID = " + ID + ";", _dbConnect);
                _dbCommand.ExecuteNonQuery();
            }
            catch (Exception e) { utils.DebugMessage(e.ToString()); return; }
        }

        public static bool insertDescrizioneFascicolo(int idFascicolo, ListBox lsbFasi)
        {
            using (SQLiteTransaction tr = _dbConnect.BeginTransaction())
            {
                //elimino le vecchie descrizioni
                try
                {
                    _dbCommand = new SQLiteCommand("delete from descFascicolo where idFascicolo = " + idFascicolo + ";", _dbConnect);
                    _dbCommand.ExecuteNonQuery();
                }
                catch (Exception e) { utils.DebugMessage(e.ToString()); tr.Rollback(); return false; }

                try
                {
                    int i = 0;
                    foreach (StackPanel sp in lsbFasi.Items)
                    {
                        KeyValuePair<int, string> selOp = (KeyValuePair<int, string>)((ComboBox)sp.Children[1]).SelectedItem;

                        if (!insQuery("insert into descFascicolo(ordine, operatore, descrizione, idFascicolo) values(?, ?, ?, ?);", new object[] {
                               i,
                               selOp.Value,
                               ((TextBox) sp.Children[0]).Text,
                               idFascicolo
                            }))
                        {
                            tr.Rollback();
                            return false;
                        }

                        i++;
                    }
                }
                catch (Exception e) { utils.DebugMessage(e.ToString()); tr.Rollback(); return false; }

                tr.Commit();
                return true;
            }
        }

        public static SQLiteDataReader getDescFascicolo(int idFascicolo)
        {
            try
            {
                _dbCommand = new SQLiteCommand("select * from descFascicolo where idFascicolo = " + idFascicolo + ";", _dbConnect);
                return _dbCommand.ExecuteReader();
            }
            catch (Exception e) { utils.DebugMessage(e.ToString()); return null; }
        }

        public static SQLiteDataReader getFascicoliLavoro(int idLavoro)
        {
            try
            {
                _dbCommand = new SQLiteCommand("select f.ID, f.nome as Nome, f.dispositivo as Dispositivo from fascicolo f join fascicoliLavori fl on fl.idFascicolo = f.ID where fl.idLavoro = " + idLavoro + ";", _dbConnect);
                return _dbCommand.ExecuteReader();
            }
            catch (Exception e) { utils.DebugMessage(e.ToString()); return null; }
        }

        public static bool insertFascicoloLavoro(int idFascicolo, int idLavoro)
        {
            return notCatchingInsQuery("insert into fascicoliLavori(idFascicolo, idLavoro) values(?, ?);",
                new object[] {
                    idFascicolo,
                    idLavoro
                });
        }

        public static void removeFascicoloLavoro(int idFascicolo, int idLavoro)
        {
            try
            {
                _dbCommand = new SQLiteCommand("delete from fascicoliLavori where idFascicolo = " + idFascicolo + " and idlavoro = " + idLavoro + ";", _dbConnect);
                _dbCommand.ExecuteReader();
            }
            catch (Exception e) { utils.DebugMessage(e.ToString()); }
        }

        public static void fascicoloFillMissingFascicoloDati(ref Dati9342 dati, int idFattura)
        {
            string query = "select f.nome || ' - ' || f.dispositivo || ' - x' || count(distinct l.ID) as fascicolo , d.descrizione, d.operatore, c.nomeStudio as cliente from descFascicolo d join fascicolo f on d.idFascicolo = f.ID join fascicoliLavori fl on fl.idFascicolo = f.ID join lavoro l on fl.idLavoro = l.ID join bolla b on b.idLavoro = l.ID join cliente c on l.idCliente = c.ID where b.idFattura = " + idFattura + " group by f.ID, d.ID;";

            SQLiteDataReader read;
            try
            {
                _dbCommand = new SQLiteCommand(query, _dbConnect);
                read = _dbCommand.ExecuteReader();
            }
            catch (Exception e) { utils.DebugMessage(e.ToString()); return; }

            

            dati.items = new List<Dati9342Item>();
            string lastFasc = null;
            while (read.Read())
            {
                if (dati.cNomeStudio == null) dati.cNomeStudio = Convert.ToString(read["cliente"]);

                Dati9342Item item = new Dati9342Item();
                string fasc = Convert.ToString(read["fascicolo"]);

                item.fascicolo = fasc != lastFasc ? fasc : "";
                item.descrizione = Convert.ToString(read["descrizione"]);
                item.operatore = Convert.ToString(read["operatore"]);

                dati.items.Add(item);
                lastFasc = fasc;
            }
        }
    }
}
