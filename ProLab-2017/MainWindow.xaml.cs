﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace ProLab_2017
{
    static class MainWindowInstanceProvider
    {
        public static MainWindow mainWindow;
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static windowListino wListino = null;
        public static windowLaboratorio wLaboratorio = null;
        public static windowCliente wCliente = null;
        public static windowLavori wLavoro = null;
        public static windowListLavori wListLavori = null;
        public static windowPlanning wPlanning = null;
        public static windowCompilaFattura wCompilaFattura = null;
        public static windowListFattura wListFattura = null;
        public static windowGestioneProtocolli wGestioneProtocolli = null;

        public MainWindow()
        {
            InitializeComponent();

            MainWindowInstanceProvider.mainWindow = this;

            //controllo password
            Regex passRegex = new Regex(@"^[A-z0-9,.;:!?$%&\-_àèìòù ]{4,}$");
            string pass = dbInterface.dbVarGet("dbPassword");
            if (pass == null)
            { //imposto la password
                do
                {
                    string p;
                    do
                    {
                        p = Dialog.InputPasswordBox("Impostare una password per la banca dati\n\nATTENZIONE:\nNon è possibile recuperare la password nel caso essa venga dimenticata");

                        if (!passRegex.IsMatch(p))
                        {
                            Dialog.MessageBox("La password deve essere lunga almeno 4 caratteri. Caratteri Validi: A-z 0-9 ,.;:!?$%&-_àèìòù ");
                            continue;
                        }

                        if (p != Dialog.InputPasswordBox("Conferma password"))
                        {
                            Dialog.MessageBox("Le due password non conincidono, riprovare");
                            continue;
                        }

                        break;
                    } while (true);

                    p = p == null ? "" : p;
                    if (dbInterface.dbVarSet("dbPassword", activation.md5(p)))
                    {
                        Dialog.MessageBox("Password impostata con successo");
                        break;
                    } else Dialog.MessageBox("Impossibile impostare password, riprovare");
                } while (true);                
            } else
            { //chiedo password
                int i = 0;
                do
                {
                    string insertedPass = Dialog.InputPasswordBox("Inserire la password d'accesso");
                    insertedPass = insertedPass == null ? "" : insertedPass;
                    if (activation.md5(insertedPass) != pass)
                    {
                        Dialog.MessageBox("La password inserita è errata");
                    }
                    else break;

                    i++;

                    if (i >= 3) { confirmClosing = false; this.Close(); break; }
                } while (true);                
            }
        }


        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void openWLaboratorio()
        {
            if (wLaboratorio == null) wLaboratorio = new windowLaboratorio();
            wLaboratorio.Show();
            wLaboratorio.Focus();
        }

        private void mniListino_Click(object sender, RoutedEventArgs e)
        {
            if (!dataInterface.isRegistered)
            {
                Dialog.MessageBox("ProLab non risulta attivato.\n\nInserire un codice di licenza valido.");
                openWLaboratorio();
                return;
            }

            if (wListino == null) wListino = new windowListino();
            wListino.Show();
            wListino.Focus();
        }

        private void mniDati_Click(object sender, RoutedEventArgs e)
        {
            openWLaboratorio();
        }


        DataTable dataTableLavoriRecenti;
        private void dtgLavoriRecentiRefresh()
        {
            dataTableLavoriRecenti = new DataTable();
            SQLiteDataReader dr = dbInterface.getListLavori(-1, -1, -1, false, false, true);
            dataTableLavoriRecenti.Load(dr);
            dtgLavoriRecenti.DataContext = dataTableLavoriRecenti.DefaultView;
        }

        DataTable dataTableScadenzeOggi;
        private void dtgScadenzeOggiRefresh()
        {
            dataTableScadenzeOggi = new DataTable();
            SQLiteDataReader dr = dbInterface.getScadenzeLavori();
            dataTableScadenzeOggi.Load(dr);
            dtgScadenzeOggi.DataContext = dataTableScadenzeOggi.DefaultView;
        }

        public void refresh(bool refreshLavoriRecenti = true)
        {
            //dati utente
            tblIntestazione.Text = dataInterface.Data.intestazione;
            tblData.Text = DateTime.Now.ToString("dd/MM/yyyy");

            //lavori recenti
            if (refreshLavoriRecenti)
            {
                dtgLavoriRecentiRefresh();
                dtgScadenzeOggiRefresh();
            }
                
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            utils.MainWindowInstance = this;
            refresh();
        }

        private void mniClienti_Click(object sender, RoutedEventArgs e)
        {
            if (!dataInterface.isRegistered)
            {
                Dialog.MessageBox("ProLab non risulta attivato.\n\nInserire un codice di licenza valido.");
                openWLaboratorio();
                return;
            }

            if (wCliente == null) wCliente = new windowCliente();
            wCliente.Show();
            wCliente.Focus();
        }

        private void mniNuovoLavoro_Click(object sender, RoutedEventArgs e)
        {
            if (!dataInterface.isRegistered)
            {
                Dialog.MessageBox("ProLab non risulta attivato.\n\nInserire un codice di licenza valido.");
                openWLaboratorio();
                return;
            }

            if (wLavoro == null) wLavoro = new windowLavori();

            windowLavori.currentIDLavoro = null;
            wLavoro.Show();
            wLavoro.Focus();
        }

        private void mniCercaLavoro_Click(object sender, RoutedEventArgs e)
        {
            if (!dataInterface.isRegistered)
            {
                Dialog.MessageBox("ProLab non risulta attivato.\n\nInserire un codice di licenza valido.");
                openWLaboratorio();
                return;
            }

            if (wListLavori == null) wListLavori = new windowListLavori();
            wListLavori.Show();
            wListLavori.Focus();
        }

        private void mniGestioneProtocolli_Click(object sender, RoutedEventArgs e)
        {
            if (!dataInterface.isRegistered)
            {
                Dialog.MessageBox("ProLab non risulta attivato.\n\nInserire un codice di licenza valido.");
                openWLaboratorio();
                return;
            }

            if (wGestioneProtocolli == null) wGestioneProtocolli = new windowGestioneProtocolli();
            wGestioneProtocolli.Show();
            wGestioneProtocolli.Focus();
        }

        private void mniCompilaFattura_Click(object sender, RoutedEventArgs e)
        {
            if (!dataInterface.isRegistered)
            {
                Dialog.MessageBox("ProLab non risulta attivato.\n\nInserire un codice di licenza valido.");
                openWLaboratorio();
                return;
            }

            if (wCompilaFattura == null) wCompilaFattura = new windowCompilaFattura();
            wCompilaFattura.Show();
            wCompilaFattura.Focus();
        }

        private void mniArchivioFatture_Click(object sender, RoutedEventArgs e)
        {
            if (!dataInterface.isRegistered)
            {
                Dialog.MessageBox("ProLab non risulta attivato.\n\nInserire un codice di licenza valido.");
                openWLaboratorio();
                return;
            }

            if (wListFattura == null) wListFattura = new windowListFattura();
            wListFattura.Show();
            wListFattura.Focus();
        }

        private void btnApriLavoro_Click(object sender, RoutedEventArgs e)
        {
            if (!dataInterface.isRegistered)
            {
                Dialog.MessageBox("ProLab non risulta attivato.\n\nInserire un codice di licenza valido.");
                openWLaboratorio();
                return;
            }


            if (dtgLavoriRecenti.SelectedItems.Count <= 0)
            {
                Dialog.MessageBox("Nessun elemento selezionato");
                return;
            }
            else if (dtgLavoriRecenti.SelectedItems.Count > 1)
            {
                Dialog.MessageBox("Selezionare un solo lavoro da modificare");
                return;
            }

            foreach (DataRowView row in dtgLavoriRecenti.SelectedItems)
            {
                if (wLavoro == null) wLavoro = new windowLavori();
                windowLavori.currentIDLavoro = (int.Parse(row["ID"].ToString()));

                wLavoro.refresh(true, true, true);
                wLavoro.Show();
                wLavoro.Focus();
            }
        }

        private void openPlanning()
        {
            if (!dataInterface.isRegistered)
            {
                Dialog.MessageBox("ProLab non risulta attivato.\n\nInserire un codice di licenza valido.");
                openWLaboratorio();
                return;
            }

            if (wPlanning == null) wPlanning = new windowPlanning();
            wPlanning.Show();
            wPlanning.Focus();
        }
        private void mniPlanning_Click(object sender, RoutedEventArgs e) { openPlanning(); }
        private void btnPlanning_Click(object sender, RoutedEventArgs e) { openPlanning(); }

        private bool confirmClosing = true;
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirmClosing)
                if (Dialog.MessageBox("Sei sicuro di voler uscire dal programma?", "Prolab", System.Windows.MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                {
                    e.Cancel = true;
                }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            App.Current.Shutdown();
        }

        private void btnApriLavoroScadenzeOggi_Click(object sender, RoutedEventArgs e)
        {
            if (!dataInterface.isRegistered)
            {
                Dialog.MessageBox("ProLab non risulta attivato.\n\nInserire un codice di licenza valido.");
                openWLaboratorio();
                return;
            }


            if (dtgScadenzeOggi.SelectedItems.Count <= 0)
            {
                Dialog.MessageBox("Nessun elemento selezionato");
                return;
            }
            else if (dtgScadenzeOggi.SelectedItems.Count > 1)
            {
                Dialog.MessageBox("Selezionare un solo lavoro da visualizzare");
                return;
            }

            foreach (DataRowView row in dtgScadenzeOggi.SelectedItems)
            {
                if (wLavoro == null) wLavoro = new windowLavori();
                windowLavori.currentIDLavoro = (int.Parse(row["idLavoro"].ToString()));

                wLavoro.refresh(true, true, true);
                wLavoro.Show();
                wLavoro.Focus();
            }
        }

        private void mniEseguiBackup_Click(object sender, RoutedEventArgs e)
        {
            if (!dataInterface.isRegistered)
            {
                Dialog.MessageBox("ProLab non risulta attivato.\n\nInserire un codice di licenza valido.");
                openWLaboratorio();
                return;
            }
            
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = ".plb";
            dlg.Filter = "ProLab Files (*.plb)|*.plb";

            dlg.RestoreDirectory = true;
            dlg.InitialDirectory = dataInterface.backupFolderPath;
            dlg.FileName = DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss");

            Nullable<bool> result = dlg.ShowDialog();
            
            if (result == true)
            {
                try
                {
                    if (File.Exists(dlg.FileName))
                        File.Delete(dlg.FileName);

                    File.Copy(dbInterface.SQLITEFILE, dlg.FileName);
                } catch(Exception Ex) { utils.DebugMessage(Ex.ToString()); Dialog.MessageBox("Impossibile eseguire backup"); return; }

                Dialog.MessageBox("Backup eseguito con successo!");
            }
        }

        private void mniCaricaBackup_Click(object sender, RoutedEventArgs e)
        {
            if (!dataInterface.isRegistered)
            {
                Dialog.MessageBox("ProLab non risulta attivato.\n\nInserire un codice di licenza valido.");
                openWLaboratorio();
                return;
            }

            if (Dialog.MessageBox("ATTENZIONE:\n\nCaricando un backup la base dati corrente verrà persa.\n\nSI CONSIGLIA VIVAMENTE DI EFFETTUARE UN BACKUP PRIMA DI CARICARNE UNO NUOVO!\n\nSei sicuro di voler continuare?", "Conferma caricamento backup", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                return;

            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".plb";
            dlg.Filter = "ProLab Files (*.plb)|*.plb";

            dlg.RestoreDirectory = true;
            dlg.InitialDirectory = dataInterface.backupFolderPath;

            Nullable<bool> result = dlg.ShowDialog();
            
            if (result == true)
            {
                try
                {
                    dbInterface._dbConnect.Close();

                    if (File.Exists(dbInterface.SQLITEFILE))
                        File.Delete(dbInterface.SQLITEFILE);

                    File.Copy(dlg.FileName, dbInterface.SQLITEFILE);
                }
                catch (Exception Ex) { utils.DebugMessage(Ex.ToString()); Dialog.MessageBox("Impossibile caricare backup"); return; }

                Dialog.MessageBox("Backup caricato con successo!\n\nProLab ora effettuerà un riavvio");
                System.Windows.Forms.Application.Restart();
                System.Windows.Application.Current.Shutdown();
            }
        }
    }
}
